<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
   <meta name="Author" content="Bob Myers">
   <meta name="GENERATOR" content="Mozilla/4.73 [en] (Win98; U) [Netscape]">
   <title>Virtual Functions and Abstract Classes</title>
</head>
<body text="#000000" bgcolor="#FFFFFF">
<b><u><font size=+2>Inheritance -
Virtual Functions and Abstract Classes</font></u></b>
<br>&nbsp;
<p><b><font size=+1>A Motivational Example</font></b>
<br>&nbsp;
<br>In the student example given in the previous notes set, we may end
up with many sub-categories of Students, which translates into many derived
classes with different information in each.&nbsp; This also means that
we will probably have many different types of Grade Reports.&nbsp; It would
really be nice to be able to store one big list of Students, and then quickly
print out ALL grade reports with a loop, like this:
<pre>
  Student list[30000]; 
  .... 
  for (int i = 0; i < size; i++) 
     list[i].GradeReport(); 
</pre>

<p>However, this code is problematic, for two reasons:

<ol> 

<li>The items in the array above are Student objects, and Student is the
base class.&nbsp; These base class objects don't have the full range of
information about the sub-types.&nbsp; Furthermore, we can't store all
these different subtypes (like Grads and Undergrads) physically in an
array, because everything in an array must be the same <b>type</b> (and
more importantly, the same <b>size</b>)!</li>

<li>The GradeReport function being called is the Student class version of
it, since we are calling through objects declared as type Student.&nbsp;
This is also not the desired call.&nbsp; If the Student is a Grad, we want
to call Grad's version.&nbsp; If the Student is an Undergrad, we want to
call Undergrad's version. </li>

</ol>

<p>One solution, of course, would be to create a separate array for each
subtype.&nbsp; While this is reasonable with very small examples, it is
not realistic when there are many subtypes. Real world situations
are always larger and more complex than small textbook examples!

<p><b>A base class pointer property:</b>
<p>We can get around the first problem through a special property of 
inheritance. Normally, a pointer can only point at one type -- the type 
that's used in the pointer declaration. However, there is a special 
rule for inheritance:

<br>&nbsp;
<li>
A pointer to a base class type <b>can</b> be pointed at an object derived
from that base class!</li>

<p><br>Examples:

<pre>
 Student s; 
 Grad g; 
 Undergrad u; 

 Student *sp1, *sp2, *sp3;    // Student pointers 

 sp1 = &amp;s;    // sp1 is now pointing at Student object 
 sp2 = &amp;g;    // sp2 is now pointing at Grad object 
 sp3 = &amp;u;    // sp3 is now pointing at Undergrad object 
</pre>

<h4>Heterogeneous List:</h4>

<p>We will create what is called a <b>heterogeneous list</b> by creating a
single array of <b>pointers</b> to a base class. These pointers can be
pointed to any objects derived from that base. So, we have essentially
created a list of various types of objects, without breaking the array
rule of same types. Everything in the array <b>is</b> the same type -- a
pointer to the base class!

<pre>
 Student * list[30000];   // an array of Student pointers 

 list[0] = &amp;g;             // using the earlier Grad object 
 list[1] = &amp;u;             // undergrad object 
 list[2] = new Grad;      // dynamic Grad object 

 // we can continue adding items to the array, of any subtype of Student 
</pre>

<h3>Virtual Functions:</h3>

<p>The heterogeneous list solves our first problem, but what about the
second? Whose version of GradeReport will run? In the above example, 
we might try loading up the array, then saying:
<pre>
 for (int i = 0; i < size; i++) 
    list[i]->GradeReport(); 
</pre>

<p>However, this still calls the Student version of GradeReport, since
<b>list[i]</b> is declared as a Student pointer. The problem is due to a
concept known as <b>binding</b>.&nbsp; The function call is bound to its
definition, normally, by the compiler.&nbsp; This means it is
static.&nbsp; Since the compiler cannot possibly know exactly what we will
be pointing to with these Student pointers when the program runs, it
cannot guess which version we really intend (Grad, Undergrad, etc). Its
assumption will be to use the type from the pointer declaration 
(<tt>Student *</tt>). So, we need some way of making this binding occur at 
run time (i.e. <b>dynamic</b>).

<p>The keyword <b>virtual</b> will give do this. To override a
base class function when it is called through a pointer, so that it will
instead run the target's version of the function, declare the base function
to be <b>virtual</b>.

<pre>
 class Student 
 { 
   public: 
      virtual void GradeReport(); 
   ... 
 }; 
</pre>

<p>Now, when GradeReport is called through a base class pointer, the program
will look at the target of the pointer and run the appropriate version
of GradeReport:

<pre>
 Student *sp1, *sp2, *sp3; 
 Student s;    Grad g;     Undergrad u; 
 sp1 = &amp;s; 
 sp2 = &amp;g; 
 sp3 = &amp;u; 

 sp1->GradeReport();    // runs Student's version 
 sp2->GradeReport();    // runs Grad's version 
 sp3->GradeReport();    // runs Undergrad's version 
</pre>

<p>So, our loop will now do what we want. With the heterogeneous
list, and the use of virtual functions, we can put all Students in one
list, and print all grade reports this way:

<pre>
 for (int i = 0; i < size; i++) 
    list[i]->GradeReport(); 
</pre>

<b>Note:</b> Virtual functions are not specific to this example of the 
heterogeneous list. This is simply a nice example that 
helps motivate the topic with an easy and common application of virtual 
functions. Virtual functions can be used any time that we want to call a 
function through a pointer (to a base class type) that is pointing at a 
derived object.

<br>&nbsp;

<h3>Abstract Classes</h3>

<p>In some situations, the base class is so abstract that we really don't
want to build objects out of it. For instance, class <tt>Student</tt>
might not have enough information for the full grade reports -- we need to
know what <b>kind</b> of student (i.e. which subtype). If you do not want
to actually do anything in the Student's version of GradeReport, you are
allowed to leave out its function definition altogether. Above, we had:

<pre>
  virtual void GradeReport(); 
</pre>

<p>This function CAN be given its own definition, but if we desire that
this virtual function be left un-implemented for the Student class itself
(perhaps because there's nothing this version can do), then we can put
<tt>"=0"</tt> on the function declaration. This tells the compiler that
this function declaration will <b>not</b> have a corresponding definition.

<pre>
 virtual void GradeReport()=0; 
</pre>

<p>Such a function is known as a <b>pure virtual function</b>. This is not 
required when making a function virtual, but it is often done when the 
base class version of the function really has no work of its own to do. 

<p>Any class that has at least one <b>pure virtual function</b> is known 
as an <b>abstract class</b>.  An abstract class <i>cannot be 
instantiated</i> (i.e. you cannot build an object of this class type).  
Abstract classes are generally used as base classes for inheritance 
hierarchies, and they are intended only as a place to put data and 
functions common to classes derived from them, as well as a place for 
virtual functions. Abstract classes can still be used to build pointers, 
though, which is useful when taking advantage of virtual functions.  

<p>Example:  Suppose we have an abstract class called 
<tt><b>Shape</b></tt>.  Of the following statements, the first is not 
legal, and the second one is legal.

<pre>
 Shape s;       // illegal declaration, since Shape is an abstract class
 Shape * sptr;  // legal declaration.  sptr may point to derived objects
</pre>

<br>
<h3>
<a href="../examples/inher/employee">Employee Example:</a></h3>
This example presents a hierarchy of derived classes that is based on a
class called Employee.&nbsp; The goal is a program that stores Employee
information and handles the printing of their paychecks.&nbsp; Note the
use of the virtual PrintCheck function, and the building of the heterogeneous
list in the sample main program.
<br>&nbsp;

</body>
</html>
