//--------------- MAIN.CPP ---------------

// Driver routine to test the Fraction class 

#include <iostream>		// for cout
#include "frac.h"		// for Fraction declarations

using namespace std;

int main()
{
	// Try all three possible fraction constructors
	// and the input/output routines.
	
	// These declarations use the default constructor
	Fraction f1, f2;

	// These declarations use the constructor with parameters
	Fraction f3(3,4), f4(6);

	cout << "** Making statement Fraction f5 = f3" << endl;
	Fraction f5 = f3;	// constructor?  (copy constructor)

 	cout << "** Making statement f1 = f4" << endl;
	f1 = f4;		// assignment operator should run

	cout << "* Calling f1 = f2 + f3" << endl;
        f1 = f2 + f3;


/*
*/

	cout << "\n The fraction f1 is ";
	f1.Show();
	
	cout << "\n The fraction f2 is ";
	f2.Show();

	cout << "\n The fraction f3 is ";
	f3.Show();

	cout << "\n The fraction f4 is ";
	f4.Show();
	
	cout << "Goodbye!\n";

	return 0;
}
