class Temperature
{
public:
   Temperature();	// default -- initialize temp to 0 Celsius
   Temperature(double d, char sc = 'C');	// init temp to given params
   ~Temperature();			// destructor

   // accessors
   char GetScale() const;
   double GetDegrees() const;
   
   // mutator (Set) function -- sets temp to given params
   //  validates data -- returns true for success, false otherwise
   bool Set(double d, char sc);
			
   // Standard I/O functions
   void Input();			// keyboard input of a temperature
   void Show() const;

   // other functions
   bool Convert(char sc);		// convert temp to scale sc
					// true for success, false failure

private:
   double degrees;	// must be > 0 Kelv (or equivalent)
   char scale;		// can ONLY be k, K, f, F, c, C

   bool IsValid(double d, char sc);	// return true if parameters
					//  represent valid data

};


