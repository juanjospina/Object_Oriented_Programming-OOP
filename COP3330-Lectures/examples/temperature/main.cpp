#include <iostream>
#include "temp.h"

using namespace std;

int main()
{
   Temperature t1;		// use default constructor
   cout << "t1 = ";
   t1.Show();			// expect:  0 Celsius
   cout << '\n';

   Temperature t2(34.6, 'F');	// invokes the constructor with parameters
   cout << "t2 = ";
   t2.Show();			// expect: 34.6 Fahrenheit
   cout << '\n';

   Temperature t3(12.5);	// this should be 12.5 Celsius
   cout << "t3 = ";
   t3.Show();			// expect: 12.5 Celsius
   cout << '\n';





   return 0;
}
