#include <iostream>
#include "temp.h"

using namespace std;

/*
   double degrees;	// must be > 0 Kelv (or equivalent)
   char scale;		// can ONLY be k, K, f, F, c, C
*/


Temperature::Temperature()
// default -- initialize temp to 0 Celsius
{
   degrees = 0;
   scale = 'C';
}

Temperature::Temperature(double d, char sc)
// init temp to given params
{
   if (Set(d, sc) == false)
   {
	degrees = 0;
 	scale = 'C';
   }
}	

Temperature::~Temperature()
{

}

char Temperature::GetScale() const
{
   return scale;
}

double Temperature::GetDegrees() const
{
   return degrees;
}
   
bool Temperature::Set(double d, char sc)
// mutator (Set) function -- sets temp to given params
//  validates data -- returns true for success, false otherwise
{
   if (IsValid(d, sc))
   {  degrees = d;
      scale = sc;
      return true;
   }

   return false;
}

			
void Temperature::Input()
{

}

void Temperature::Show() const
// print temperature in format like:  0 Cels
{
   cout << degrees << ' ';
   switch(scale)
   {
	case 'k':
	case 'K':
		cout << "Kelv";
		break;
	case 'f':
	case 'F':
		cout << "Fahr";
		break;
	case 'c':
	case 'C':
		cout << "Cels";
   }
}

bool Temperature::Convert(char sc)
// convert temp to scale sc -- true for success, false failure
{

}

bool Temperature::IsValid(double d, char sc)
// validate the incoming temperature (true for good, false for bad)
{

}
