#include <iostream>

using namespace std;

int main()
{
   int x;
   cout << "Please type a number from 1 - 10: ";
   cin >> x;

   if (x < 1 || x > 10)
   {	cerr << "** Warning: Invalid input.  Defaulting to 0\n";
	x = 0;
   }

   cout << "x = " << x << '\n';

   return 0;
}
