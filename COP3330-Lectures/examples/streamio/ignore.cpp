// Example of ignore() function

#include <iostream>

using namespace std;

int main()
{
   cout << "Demonstrating example of ignore() function\n";

   int age;
   char comment[31];
   cout << "Enter comment (30 char max): ";
   cin.get(comment, 31);
   cin.ignore(80, '\n');	// skip the remaining characters, up to 80
   cout << "Enter age: ";
   cin >> age;

   cout << "\nResults:\n" << "  Comment = " << comment << "\n  Age = "
	<< age << '\n';

   return 0;
}
