#include <iostream>

using namespace std;

int main()
{
   int input;
   cout << "Enter an integer: ";
   cin >> input;		// let's try a char instead

   while (cin.fail())		// check fail bit
   {
	input = 0;
	cin.clear();		// clear the stream error bits
	cin.ignore(80,'\n');    // clear the input buffer (to the newline)

	cout << "Try again: ";
	cin >> input;		// re-enter
   }

   cout << "The integer is: " << input << '\n';

   return 0;
}
