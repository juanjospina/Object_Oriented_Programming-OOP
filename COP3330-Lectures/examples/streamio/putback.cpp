// Example of putback() function

#include <iostream>

using namespace std;

int main()
{
   cout << "Demonstrating example of putback() function\n";

   int age;
   char firstName[21], temp;
   cout << "Enter first name and age (either order OK): ";
   cin.get(temp);		// extract first character
   cin.putback(temp);		// put it back on the stream
   if (temp >= '0' && temp <= '9')	// if 1st character is a digit
	cin >> age >> firstName;	// age is read first
   else
	cin >> firstName >> age;	// name is read first

   cout << "\nResults:\n" << "  First name = " << firstName << "\n  Age = "
	<< age << '\n';

   return 0;
}
