#include <iostream>  

using namespace std;

int Sum(int a, int b);			// function declarations
void Twice(int, int);			// DECLARE before use

int main()
{
	int x = 5, y = 8;

	cout << "Initial values of variables:\n";
	cout << "\tx = " << x << "\ty = " << y << '\n';

	int result;			// new declaration!
	result = Sum(10, 25);		// function call

	cout << "The sum of 10 and 25 is:  " << result << '\n';
	cout << "The sum of x and y is:  " << Sum(x,y) << "\n\n";

	cout << "Calling the function Twice(x,y)\n";

	Twice(x,y);			// what is the effect of this?
	
	cout << "The new values of x and y are:\n";
	cout << "\tx = " << x << "\ty = " << y << '\n';
	cout << "Goodbye!\n";
}  // end main

int Sum(int a, int b)
{
	return (a + b);
}

void Twice(int a, int b)
// So what exactly does this function do?
{
	a *= 2;
	b *= 2;
}
