#include <iostream>

using namespace std;

class A
{
public:
	A()  {  cout << "Running constructor A()" << endl;  };
	~A() {  cout << "Running destructor ~A()" << endl;  };
};

class B : public A
{
public:
	B()  {  cout << "Running constructor B()" << endl;  };
	~B() {  cout << "Running destructor ~B()" << endl;  };
};

class C : public B
{
public:
	C()  {  cout << "Running constructor C()" << endl;  };
	~C() {  cout << "Running destructor ~C()" << endl;  };
};

int main()
{
cout << "Starting test\n\n";

A aobject;

  {
     cout << "Declaring next object of type B" << endl;

     B bobject;

     {
        cout << "Declaring next object of type C" << endl;
        C cobject;
	cout << "Object C now going out of scope" << endl;
     }

     cout << "Object B now going out of scope" << endl;
   }

   cout <<"Object A now going out of scope" << endl;

   return 0;
}
