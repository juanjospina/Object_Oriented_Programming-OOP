class GeometricObject
{
public:
   void Draw();
   void Erase();
   void Move(int h, int v);

   int top,
       left,
       bottom,
       right;
};

class Two_D_Object : public GeometricObject
// derived from GeometricObject
{
public:
   int fillPattern;
};

class Rectangle : public Two_D_Object
{
public:
   int length, width;
};
