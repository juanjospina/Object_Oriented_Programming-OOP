#include <iostream>
#include <cctype>
#include "a.h"

using namespace std;

int main()
{
   char ch;

   cout << "  \n\n* Testing with heterogeneous list\n";
   cout << "  * Building array of 5 pointers of type (A* )\n";
   A* list[5];
   
   for (int i = 0; i < 5; i++)
   {
	cout << "What type of object would you like to attach to list["
		<< i << "] (A/B/C/D/E)?  ";
	cin >> ch;
	ch = toupper(ch);
	cout << " \n* Building object of requested type\n";
	switch(ch)
	{
	   case 'A':	list[i] = new A;	break;
	   case 'B':	list[i] = new B;	break;
	   case 'C':	list[i] = new C;	break;
	   case 'D':	list[i] = new D;	break;
	   case 'E':	list[i] = new E;	break;
	   default:     cout << " \n** Illegal choice.  Try again\n";
			i--;
	}
   }

   cout << " \n* Now calling list[i]->Print() for all 5 array slots\n";
   for (int i = 0; i < 5; i++)
   {	list[i]->Print();
	cout << '\n';
   }

   return 0;
}
