class A
{
public:
   A();
   ~A();
   virtual void Print();

};

class B : public A
{
public:
   B();
   ~B();
   void Print();
};

class C : public B
{
public:
   C();
   ~C();
   void Print();

};

class D : public C
{
public:
   D();
   ~D();
   void Print();

};

class E : public C
{
public:
   E();
   ~E();

};
