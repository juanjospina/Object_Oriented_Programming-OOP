#include <iostream>
#include "a.h"

using namespace std;

int main()
{
   cout << "  \n* Building object of type A \n";
   A objA;
   cout << "  \n* Building object of type B \n";
   B objB;
   cout << "  \n* Building object of type C \n";
   C objC;
   cout << "  \n* Building object of type D \n";
   D objD;
   cout << "  \n* Building object of type E \n";
   E objE;

   cout << "  \n* Calling objA.Print()\n";
   objA.Print();
   cout << "  \n* Calling objB.Print()\n";
   objB.Print();
   cout << "  \n* Calling objC.Print()\n";
   objC.Print();
   cout << "  \n* Calling objD.Print()\n";
   objD.Print();
   cout << "  \n* Calling objE.Print()\n";
   objE.Print();

   cout << "  \n* Starting tests with pointers\n";

   cout << "  * Building 5 pointers of type (A* )\n";
   A *ptr1, *ptr2, *ptr3, *ptr4, *ptr5;

   cout << "  * Attaching pointers to objects\n";
   ptr1 = &objA;
   ptr2 = &objB;
   ptr3 = &objC;
   ptr4 = &objD;
   ptr5 = &objE;

   cout << "  \n* Calling ptr1->Print()\n";
   ptr1->Print();
   cout << "  \n* Calling ptr2->Print()\n";
   ptr2->Print();
   cout << "  \n* Calling ptr3->Print()\n";
   ptr3->Print();
   cout << "  \n* Calling ptr4->Print()\n";
   ptr4->Print();
   cout << "  \n* Calling ptr5->Print()\n";
   ptr5->Print();
   cout << "\n";

   return 0;
}
