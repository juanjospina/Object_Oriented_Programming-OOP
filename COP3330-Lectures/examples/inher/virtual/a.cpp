#include <iostream>
#include "a.h"

using namespace std;

A::A()
{
   cout << "Running constructor A() \n";
}

A::~A()
{
   cout << "Running destructor ~A() \n";
}

void A::Print()
{
   cout << "Running A::Print() \n";
}

   
B::B()
{
   cout << "Running constructor B() \n";
}

B::~B()
{
   cout << "Running destructor ~B() \n";
}

void B::Print()
{
   cout << "Running B::Print() \n";
}

C::C()
{
   cout << "Running constructor C() \n";
}

C::~C()
{
   cout << "Running destructor ~C() \n";
}

void C::Print()
{
   cout << "Running C::Print() \n";
}

D::D()
{
   cout << "Running constructor D() \n";
}

D::~D()
{
   cout << "Running destructor ~D() \n";
}

void D::Print()
{
   cout << "Running D::Print() \n";
}

E::E()
{
   cout << "Running constructor E() \n";
}

E::~E()
{
   cout << "Running destructor ~E() \n";
}

