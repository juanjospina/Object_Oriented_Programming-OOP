// file to demonstrate basic pointers.  Note the addresses that print out
// in different situations

#include <iostream>

using namespace std;

int main()
{
   int n = 5;
   int *p;			// allocate pointer p
   int *r;			// allocate pointer r

   cout << endl;
   cout << "p = " << p << endl;		// show contents of p (an address)
   cout << "r = " << r << endl;		// show contents of r

   cout << "p located at " << &p << endl;	// show address of p
   cout << "r located at " << &r << endl;	// show address of r
   cout << "n located at " << &n << endl;	// show address of n

//   cout << "p points to " << *p << endl;	// valid or not?
   cout << "r points to " << *r << endl;	// valid or not?

   cout << "Now pointing p at n" << endl;
   p = &n;

   cout << endl;	
   cout << "n = " << n << endl;
   cout << "*p = " << *p << endl;
   cout << "Address of n = " << &n << endl;
   cout << "p = " << p << endl;

// creation of array
   int a[10] = {1,2,3,4,5,6,7,8,9,10};

   cout << "\nAddress of array a = " << a << endl;
   for (int i = 0; i < 10; i++)
   {
	cout << "a[" << i << "]:   ";
	cout << "Value = " << a[i] << '\t';
	cout << "Address = " << &(a[i]) << endl;
   }
}

