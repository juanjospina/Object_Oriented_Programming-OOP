// declaration of a class of Circle objects

class Circle
{
public:
   void SetCenter(double x, double y);
   void SetRadius(double r);
   void Draw();
   double AreaOf();

private:
   double radius;
   double center_x;
   double center_y;
};
