// declaration for class of TimeType objects

class TimeType
{
public:
   void Set(int, int, int);	// set the time
   void Increment();		// increment the timer by one second
   void Display();		// output the time

   // accessor functions
   int GetHours();
   int GetMinutes();
   int GetSeconds();

private:
   int hours;
   int minutes;
   int seconds;
};
