#include <iostream>  

using namespace std;

int main() 
{

int number;  // integer to be input by user

//execute the following loop until 0 or a negative number is entered	
do 
{
	cout<<"Enter number (0 or less to quit)"<<endl;

	cin>>number;  //take user input

	if (number >= 1) 	
		cout<<"I am positive\n";  //if positive print message
}
while (number >= 1); //if number is 1 or higher loop, otherwise exit loop

cout<<"done"<<endl;

return 0;
}
