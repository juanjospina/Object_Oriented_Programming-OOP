#include <iostream>  

using namespace std;

int Valid(int);

int main() {

 int num;        // user inputted integer
 int value = 4;  // predetermined secret value
 int retval;     // return value from function Valid
	
 int done = 0;

 while (!done) 
 {
	cout<<"Pick a number between 1 and 10\n";
	cin>>num;

	retval = Valid(num); //function call to valid, passes value of num
			     //return value from Valid is stored in retval
	if (retval == 0) 
	{
		cout<<"invalild selection, try again\n\n";
	}
		
	if (num == value)
	{
		cout<<"You guessed correctly\n";
		done = 1;
	}
	else if (retval == 1)	//if valid selection and guess is wrong
		cout<<"You guessed wrong\n\n"; //print this statement

	
 } //end while

 return 0;
}

int Valid(int number) {
 if ( (number > 10) || (number < 1) ) 
	return 0; //if number is less than 1 or greater than 10
 
 else return 1;   //number is between 1 and 10
}
	
