#include <iostream>  

using namespace std;

int main() {

	int num;
	int value = 4;

	cout<<"Pick a number between 1 and 10\n";
	cin>>num;

	if ( (num > 10) || (num < 1) ) 
	{
		cout<<"bad selection\n";
		return 0;
	}

	if (num == value)
		cout<<"You guessed correctly\n";
	else 
		cout<<"You guessed wrong\n";

	cout<<"The number is "<<value<<endl;

	return 0;
}
