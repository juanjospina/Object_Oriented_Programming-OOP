// Example of nested for loops

#include <iostream>  

using namespace std;

int main()
{
   int i, j;
   cout << '\n';

   int count = 0;

   for (i = 0; i < 5; i++)
   {	for (j = 0; j < 5; j++)
	{
	   count++;
	   cout << "i = " << i << "\tj = " << j << "\tcount = " << count;
	   cout << '\n';
	}
   }

}
