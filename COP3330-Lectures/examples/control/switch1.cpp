// Bob Myers
// File:  switch1.cpp
//
// This is syntactically correct -- but logically wrong

#include <iostream>

using namespace std;

int main()
{
   int score;
   char grade;
   cout << "\nPlease enter test grade: ";
   cin >> score;

   switch (score / 10)
   {
	case 10:		// score is 100 - 109
	   grade = 'A';
	case 9:			// score is 90 - 99
	   grade = 'A';
	case 8:			// score is 80 - 89
	   grade = 'B';
	case 7:			// score is 70 - 79
	   grade = 'C';
	case 6:			// score is 60 - 69
	   grade = 'D';
	default:
	   grade = 'F';		// anything below 60 flunks
   }

   cout << "\nStudent grade = " << grade << endl;

   return 0;
}
