// short routine to illustrate an example in class involving scope

#include <iostream>  

using namespace std;

int main()
{
   cout << "\nStarting Program\n";
   {
	int x = 5;			// declare new variable

	cout << "x = " << x << '\n';
	{
	   int x = 8;
	   cout << "x = " << x << '\n';
	}
	cout << "x = " << x << '\n';
	{
	   x = 3;
	}
	cout << "x = " << x << '\n';
   }
}



