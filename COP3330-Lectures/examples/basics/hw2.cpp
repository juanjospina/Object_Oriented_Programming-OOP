#include <iostream>

using std::cout;
using std::endl;

int main()
{
   cout << "Hello, ";
   cout << "world!";
   cout << endl;

   return 0;
}
