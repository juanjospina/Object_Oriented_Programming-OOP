#include <iostream>

using namespace std;

int main()
{
	int x = 5, y = 8;

	cout << "Hello, World!\n\n";

	cout << "Initial values of variables:\n";
	cout << "\tx = ";
	cout << x;
	cout << "\ty = ";
	cout << y;
	cout << '\n';

	cout << "Now please enter a new value for x: ";
	cin >> x;
	cout << "Now please enter a new value for y: ";
	cin >> y;
	cout << "New values of x and y are:\n";
	cout << "\tx = " << x << " \ty = " << y << '\n';
	
	int result;			// new declaration!
	result = x + y;

	cout << "The sum of " << x << " and " << y << " is " << result;
	cout << "\nGoodbye!\n";
}
