//      comma.cpp
//      Bob Myers
//
//      replace '\n' characters in a file with ','
//	writes result to an output file

#include <iostream>
#include <iomanip>    // for setw
#include <fstream>    // for ifstream, ofstream

using namespace std;

int main()
{
        char filename[25];      // initialize a string for filename
	char ofilename[25];	// initialize a string for ofilename

	char ch;		// initialize a character variable

        ifstream in1;           // initialize a file input
	ofstream out1;		// initialize a file output

	do
	{
		in1.clear();
        	cout << "Please enter the name of the input file.\n";
        	cout << "Filename:  ";
        	cin >> setw(25) >> filename;
	
        	in1.open(filename);
        	if (!in1)
        	{
                	cout << "That is not a valid file.  Try again!\n";
        	}
        } while (!in1);

	do
	{
		out1.clear();
        	cout << "Please enter the name of the output file.\n";
        	cout << "Filename:  ";
        	cin >> setw(25) >> ofilename;
	
        	out1.open(ofilename);
        	if (!out1)
        	{
                	cout << "That is not a valid file.  Try again!\n";
        	}
        } while (!out1);

//
/*
	while (in1.get(ch))
	{	
		if (ch == '\n')
			out1 << ',';
		else
			out1 << ch;
	}

/****** Easier-to-read version of the while loop: *****************
*/
	while (!in1.eof())
	{
		in1.get(ch);

		if (ch == '\n')
			out1 << ',';		// convert to comma
		else
			out1 << ch;
	}

// ************ end alternate version ****************************** 

        in1.close();
	out1.close();

        cout << "Processing complete\n";
	return 1;
}

