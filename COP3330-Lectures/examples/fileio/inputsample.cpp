//      Bob Myers
//
//	sample of using file input. 
//	Try this out on the file "testinput"
//	Notice the order of items in the file, and compare that with the
//		order of reads using the ifstream object called in1

#include <iostream>
#include <iomanip>    // for setw
#include <fstream>    // for ifstream, ofstream

using namespace std;

int main()
{
        char filename[25];      // initialize a string for filename

	int numList[20];
	char name[30];
	double x;
	char word1[15];
	char word2[15];
	char word3[15];


        ifstream in1;           // initialize a file input

	do
	{
		in1.clear();
        	cout << "Please enter the name of the input file.\n";
        	cout << "Filename:  ";
        	cin >> setw(25) >> filename;
	
        	in1.open(filename);
        	if (!in1)
        	{
                	cout << "That is not a valid file.  Try again!\n";
        	}
        } while (!in1);

// get some input

	in1 >> x;			// first reads a double
	in1.get();			// eats up the newline
	in1.getline(name, 30);		// then a string (up to '\n')
	for (int i = 0; i < 20; i++)	// then a list of integers
		in1 >> numList[i];
	in1 >> word1;
	in1 >> word2;
	in1 >> word3;			// then three words

	in1.close();

// output to screen the stuff that we read from the file

	cout << "\nThe name was " << name << endl;
	cout << "The decimal was " << x << endl;
	cout << "Here is every other number from the integer list:\n";
	for (int i = 0; i < 20; i+=2)
		cout << numList[i] << ' ';

	cout << "\nHere are the three words:\n";
	cout << '\t' << word1 << "\n\t" << word2 << "\n\t" << word3;
	cout << "\n\nInput sample complete\n";
}


