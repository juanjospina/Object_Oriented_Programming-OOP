#include <iostream>

using namespace std;

#ifndef _LIST_H
#define _LIST_H

template< class T >
class List
{
public:
  List(int s = 10);		// allocate array size s and set up empty list
  ~List();			// clean up dynamic allocation
  List(const List< T >& );	// copy constructor
  List< T >& operator=(const List< T >& );  // assignment operator overload

  void Insert(T item);		// loads "item" into list
  T GetElement(unsigned int n);	// return item at index n
  void Print(ostream& os);	// prints the list

private:
  T* data;			// pointer to dynamic array
  int size;			// number of items
  int max;			// allocated array size

  void Clone(const List< T >& );// for use in copies
  void Resize(int newsize);	// adjust allocation

};

#endif
