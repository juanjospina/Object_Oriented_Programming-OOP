class Flex
{
   friend ostream& operator<< (ostream& s, const Flex& f);

public:
   Flex();			// default constructor
   Flex(char *);		// constructor with parameters
   ~Flex();			// destructor (not specifically required)
   void cat(Flex & f);		// cat function -- concatenation

private:
   char * str;			// variable length string
   int size;
};
