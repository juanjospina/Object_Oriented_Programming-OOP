#include <iostream.h>
#include <string.h>
#include "flex.h"

ostream& operator<< (ostream& s, const Flex& f)
{
   s << '*' << f.str << '*';
   return s;
}

Flex::Flex()
{
   size = 2;			// size includes space for null character
   str = new char[size];
   strcpy(str," ");
}

Flex::Flex(char * s)
{
   size = strlen(s) + 1;
   str = new char[size];
   strcpy(str,s);
}

Flex::~Flex()
// not specifically required by the specs, but a good idea to have this
{
   delete [] str;
}

void Flex::cat(Flex & f)
// this function can also be made easier through the use of the
// strcat library function for concatenating strings.
// dyanamic reallocation still required, though.
{
   int oldsize = size - 1;		// oldsize is size of old string
					// (not including null character)
   size = size + strlen(f.str);
   char * temp = new char[size];
   strcpy(temp,str);			// copy this string to temp
   for (int i = 0; i < f.size; i++)
	temp[oldsize+i] = f.str[i];	// concatenate f.str to temp

   delete [] str;				// delete old array
   str = temp;				// set str to new one
}

