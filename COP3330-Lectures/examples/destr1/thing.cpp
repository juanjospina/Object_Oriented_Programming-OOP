// test class to illustrate constructors and destructors

#include <iostream>
#include "thing.h"

using namespace std;

// member function definitions

Thing::Thing()
{
   data = 0;
   cout << "Running default constructor: data = " << data << '\n';

}

Thing::Thing(int x)
{
   data = x;
   cout << "Running constructor (with parameter): data = " << data << '\n';
}

Thing::~Thing()
{
   cout << "Running destructor: data = " << data << '\n';
}

