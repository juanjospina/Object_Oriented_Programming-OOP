#include <iostream>
#include <cstring>
#include <cctype>

using namespace std;

class Flex
{

	friend ostream& operator<<(ostream& os, const Flex& f);	

	public:
	
	Flex();
	Flex(const char* c);
	void cat(const Flex& c);

	// 3 Important
	~Flex(); 			// Destructor
	Flex& operator=(const Flex& f); // assignment operator
	Flex(const Flex& f);		// Copy contructor 




	private:
	
	char* str; // pointer to dynamic allocated char array.
	int size;




};
