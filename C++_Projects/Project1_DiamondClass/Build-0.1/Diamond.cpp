/***********************************************************************************************/
// Project name:    Print Info and Draw Box Program.(Class)
// Author:		    Juan Jose Ospina Casas
// Email:			jjo11e@my.fsu.edu
// TA:			    Nan Zhao
// Course Info:	    COP3014
// Project Number:	7
// Due Date:	    12/05/2013
// Summary:		    The program displays the summary of the box specified (size, perimeter,area), and its
//					corresponding drawing.
// Output:			Prints out the summary and drawing of the box specified.
// Assumptions:		N/A
/***********************************************************************************************/

#include <iostream>
#include <iomanip>
#include <cmath>
#include "Diamond.h"

using namespace std;

// *********************************************************************
// * Member Constructor:  Box
// * Description: Creates the box, and assigns the each value received,
// *			  using the respective functions of the class.
// * Parameter List: int sizebox = integer received by constructor(Size of the box)
// *                 char boardbox = char received by constructor(board of the box)
// *                 char fillbox = char received by constructor(fill of the box)
// * Author: Juan Jose Ospina
// * Date: July 9, 2013
// *********************************************************************

Diamond::Diamond(int s, char b, char f)
{
    sizeofdiamond = s;
	SetBorder(b);
	SetFill(f);

	if(s<1)
			sizeofdiamond = 1;
	if(s>39)
			sizeofdiamond = 39;

}

// *********************************************************************
// * Member Function:  GetSize
// * Description: Gets the size received in the constructor and assigns it to
// *			  variable realsize. Initialize the variable.
// * Parameter List: int s : integer received.
// * Author: Juan Jose Ospina
// * Date: July 9, 2013
// *********************************************************************
int Diamond::GetSize()
{
    return sizeofdiamond;
}

// *********************************************************************
// * Member Function:  Perimeter
// * Description: Calculates the perimeter of the box.
// * Parameter List: N/A
// * Author: Juan Jose Ospina
// * Date: July 9, 2013
// *********************************************************************
int Diamond::Perimeter()
{
    int perimeter = sizeofdiamond * 4;
    return perimeter;
}

// *********************************************************************
// * Member Function:  Area
// * Description: Calculates the area of the box.
// * Parameter List: N/A
// * Author: Juan Jose Ospina
// * Date: July 9, 2013
// *********************************************************************
double Diamond::Area()
{
    double area = (0.5 * sizeofdiamond * sizeofdiamond) + (0.5 * sizeofdiamond * sizeofdiamond);
    return area;
}

// *********************************************************************
// * Member Function:  Grow
// * Description: Increase the size of the box by 1. Checks if by increasing,
// *              the size of the box goes out of bounds. If so, no change.
// * Parameter List: N/A
// * Author: Juan Jose Ospina
// * Date: July 9, 2013
// *********************************************************************
void Diamond::Grow()
{
    if(sizeofdiamond >= 39)
        sizeofdiamond = sizeofdiamond;
    else
        sizeofdiamond = sizeofdiamond + 1;
}

// *********************************************************************
// * Member Function:  Shrink
// * Description: Decrease the size of the box by 1. Checks if by decreasing,
// *              the size of the box goes out of bounds. If so, no change.
// * Parameter List: N/A
// * Author: Juan Jose Ospina
// * Date: July 9, 2013
// *********************************************************************
void Diamond::Shrink()
{
    if(sizeofdiamond <= 1)
        sizeofdiamond = sizeofdiamond;
    else
        sizeofdiamond = sizeofdiamond - 1;
}


// *********************************************************************
// * Member Function:  SetBorder
// * Description: Assings the respective char to the border. Sets the variable
// *              realborder to the respective char provided. If is not
// *			  between the ASCII range(33-126), assigns the default char.
// * Parameter List: char b : char received.
// * Author: Juan Jose Ospina
// * Date: July 9, 2013
// *********************************************************************
void Diamond::SetBorder(char b)
{
	if(b>=33 && b<=126)
		borderchar = b;
	else
		borderchar = '#';
}

// *********************************************************************
// * Member Function:  SetFill
// * Description: Assings the respective char to the fill. Sets the variable
// *              realfill to the respective char provided. If is not
// *			  between the ASCII range(33-126), assigns the default char.
// * Parameter List: char f: char received.
// * Author: Juan Jose Ospina
// * Date: July 9, 2013
// *********************************************************************
void Diamond::SetFill(char f)
{
	if(f>=33 && f<=126)
		 fillchar = f;
	else
		fillchar = '*';
}


// *********************************************************************
// * Member Function:  Draw
// * Description: Prints out or "Draw" the respective box, according to
// *			  the values of the variables.
// * Parameter List: N/A
// * Author: Juan Jose Ospina
// * Date: July 9, 2013
// *********************************************************************
void Diamond::Draw()
{



// Upper Part of the Triangle

   for(int i=1; i<=sizeofdiamond; i++)
    {
        for(int k=0; k<=sizeofdiamond-i; k++)
        {
            cout << " ";
        }

        cout << borderchar << " ";


        for(int j=2; j<=i-1; j++)
        {
            cout <<fillchar << " ";

        }

        cout << borderchar << " ";


        cout <<endl;

    }



    for(int i=sizeofdiamond; i>0; i--)
    {
        for(int k=sizeofdiamond; k>i; k--)
        {
            cout << " ";
        }

         cout << borderchar << " ";

        for(int j=0; j<i; j++)
        {
            cout <<fillchar << " " ;
        }

         cout << borderchar;

        cout <<endl;

    }



//    cout << setfill(borderchar) << setw(sizeofdiamond+1) << "\n";
//
//	for (int i=1; i<sizeofdiamond-1;i++)
//		  cout << sizeofdiamond << setfill(fillchar) << setw(sizeofdiamond-1)<< borderchar <<endl;
//
//	 cout << setfill(borderchar) << setw(sizeofdiamond+1) << "\n";
}

// *********************************************************************
// * Member Function:  Summary
// * Description: Prints out the Summary of the box specified. Calls the
// *              respective functions needed to provide the information.
// *			  Prints out - size, perimeter, area, drawing of the box.
// * Parameter List: N/A
// * Author: Juan Jose Ospina
// * Date: July 9, 2013
// *********************************************************************
void Diamond::Summary()
{
    cout << "The diamond size is : " << sizeofdiamond << endl;
    cout << "The perimeter is : " << Perimeter() << endl;
    cout << "The area is : " << Area() << endl;

    Draw();
}
