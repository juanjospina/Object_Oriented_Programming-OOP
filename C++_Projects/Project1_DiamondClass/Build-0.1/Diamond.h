/***********************************************************************************************/
// Project name:    Print Info and Draw Box Program.(Class Header)
// Author:		    Juan Jose Ospina Casas
// Email:			jjo11e@my.fsu.edu
// TA:			    Nan Zhao
// Course Info:	    COP3014
// Project Number:	7
// Due Date:	    12/05/2013
// Summary:		    The program displays the summary of the box specified (size, perimeter,area), and its
//					corresponding drawing.
// Output:			Prints out the summary and drawing of the box specified.
// Assumptions:		N/A
/***********************************************************************************************/

#include <iostream>
#ifndef DIAMOND_H
#define DIAMOND_H

using namespace std;

class Diamond
{
    public:

		Diamond(int s,char b='#',char f='*');   // Constructor
        int GetSize();					// Gets Size
        int Perimeter();					// Calculates Perimeter
        double Area();							// Calculates Area
        void Grow();						// Grow size by 1
        void Shrink();						// Shrink size by 1
        void SetBorder(char b);				// Set border char
        void SetFill(char f);				// Set fill char
        void Draw();						// Draw the box
        void Summary();						// Print Summary


    private:

	   // Variables
       int sizeofdiamond;
       char borderchar;
       char fillchar;


};

#endif
