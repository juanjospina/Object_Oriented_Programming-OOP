/*************************************************************/
// Project: Diamond Class Header
// Author:  Juan Jose Ospina Casas
// Email:   jjo11e@my.fsu.edu
// Course:  COP3330
// Project: 1
// Due Date:05/27/2014
//  Summary: Print the information and design of the diamond
//           specified by the user. The program has many 
//           functions as grow, shrink, setfill, set border,
//           etc. that modifies the diamond according to input
//           of the user. 
// Output:  Prints out information and design of a diamond.
// Assumptions:		N/A
/*************************************************************/

#include <iostream>

using namespace std;

class Diamond
{
	public:

		Diamond(int s,char b='#',char f='*'); // Constructor

		double Area();			// Calculates Area
		int GetSize() const;	        // Gets Size
		int Perimeter();		// Calculates Perimeter
		void SetFill(char f);           // Set fill char
		void Grow();			// Grow size by 1
		void Shrink();			// Shrink size by 1
		void SetBorder(char b);		// Set border char
		void Draw() const;		// Draw the box
		void Summary() ;		// Print Summary


	private:

		//Member Data 
		int sizeofdiamond;
		char borderchar;
		char fillchar;


};

