/**********************************************************/
// Project:  Diamond Class
// Author:   Juan Jose Ospina Casas
// Email:    jjo11e@my.fsu.edu
// Course :  COP3330
// Project:  1	
// Due Date: 05/27/2014
// Summary: Print the information and design of the diamond
// 	    specified by the user. The program has many 
// 	    functions as grow, shrink, setfill, set border,
// 	    etc. that modifies the diamond according to input
// 	    of the user. 
// Output:  Prints out information and design of a diamond.
// Assumptions:	N/A
/**********************************************************/

#include <iostream>
#include <iomanip>
#include <cmath>
#include "diamond.h"

using namespace std;

// *****************************************
// * Member Constructor: Diamond
// * Description: Constructs the Diamond object
// * Parameter List: int s - size of diamond
// 		     char b - char for border
// 		     char f - char for fill
// * Author: Juan Jose Ospina
// * Date: May 21, 2014
// *****************************************

Diamond::Diamond(int s, char b, char f)
{
	sizeofdiamond = s;
	SetBorder(b);
	SetFill(f);

	if(s<1)
		sizeofdiamond = 1;
	if(s>39)
		sizeofdiamond = 39;

}

// *****************************************
// * Member Function: GetSize
// * Description: Gets the size of one side 
// 		  of the diamond.
// * Parameter List: N/A
// * Author: Juan Jose Ospina
// * Date: May 21, 2014
// *****************************************

int Diamond::GetSize() const
{
	return sizeofdiamond;
}

// *****************************************
// * Member Function: Perimeter
// * Description: Calculates the perimeter
// 		  of the diamond.
// * Parameter List: N/A
// * Author: Juan Jose Ospina
// * Date: May 21, 2014
// *****************************************
int Diamond::Perimeter()
{
	int perimeter = sizeofdiamond * 4;
	return perimeter;
}

// *****************************************
// * Member Function:  Area
// * Description: Calculates the area of the box.
// * Parameter List: N/A
// * Author: Juan Jose Ospina
// * Date: May 21, 2014
// *****************************************

double Diamond::Area()
{
	double area = ((0.5)*sizeofdiamond*sizeofdiamond)*2;
	return area;
}

// *****************************************
// * Member Function:  Grow
// * Description: Grows the size of the diamond
// 		  by one. If it's out of bounds 
// 		  it mantains the same size.
// * Parameter List: N/A
// * Author: Juan Jose Ospina
// * Date: May 22, 2014
// *****************************************

void Diamond::Grow()
{
	if(sizeofdiamond >= 39)
		sizeofdiamond = sizeofdiamond;
	else
		sizeofdiamond = sizeofdiamond + 1;
}

// *****************************************
// * Member Function:  Shrink
// * Description: Shrink the size of the diamond
// 		  by one. If it's out of bounds
// 		  it mantains the same size.
// * Parameter List: N/A
// * Author: Juan Jose Ospina
// * Date: May 22, 2014
// *****************************************
void Diamond::Shrink()
{
	if(sizeofdiamond <= 1)
		sizeofdiamond = sizeofdiamond;
	else
		sizeofdiamond = sizeofdiamond - 1;
}

// *****************************************
// * Member Function:  SetBorder
// * Description: Sets the border of the diamond
// 		  to the char given as parameter.
// * Parameter List: char b - char given for new
// 		     border.
// * Author: Juan Jose Ospina
// * Date: May 22, 2014
// *****************************************
void Diamond::SetBorder(char b)
{
	if(b>=33 && b<=126)
		borderchar = b;
	else
		borderchar = '#';
}

// *******************************************
// * Member Function:  SetFill
// * Description: Sets the fill of the diamond
// 		  to the char given as parameter.
// * Parameter List: char f - char given for new
// 		     fill.
// * Author: Juan Jose Ospina
// * Date: May 23, 2014
// *******************************************
void Diamond::SetFill(char f)
{
	if(f>=33 && f<=126)
		fillchar = f;
	else
		fillchar = '*';
}


// ******************************************
// * Member Function:  Draw
// * Description: Draws the design of the diamond
// 		  based on the given specifications.
// * Parameter List: N/A
// * Author: Juan Jose Ospina
// * Date: May 25, 2014
// ******************************************
void Diamond::Draw() const
{

	// Upper Part of the Triangle

	for(int i=0; i<sizeofdiamond; i++) 	       // Loop for rows.
	{
		for(int k=sizeofdiamond-i; k>=0; k--)  // Loop for columns.
		{
			if(k==0)
				cout<<borderchar<<" "; //Prints border char.
			else
				cout << " ";	       // Prints blank spaces.	
		}

		for(int j=0; j<i-1; j++)              
		{                                      //Prints filling char. 
			cout <<fillchar << " ";      
		}

		if(i==0)
			cout<<" ";                    //Prints border right.
		else				   
			cout <<borderchar;

		cout <<"\n";
	}	

	//Lower Part of the Triangle

	for(int i=sizeofdiamond-2; i>=0; i--)	       
	{
		for(int k=0; k<=sizeofdiamond-i; k++)  
		{	
			if(k==sizeofdiamond-i)	      
				cout<<borderchar<<" ";
			else
				cout << " ";          
		}

		for(int numcol=0; numcol<i-1; numcol++)
		{
			cout <<fillchar <<" " ;
		}

		if(i==0)			       
			cout<<" ";
		else	
			cout << borderchar;

		cout <<"\n";
	}
}

// **********************************************
// * Member Function:  Summary
// * Description: Prints out all the information
// 		  required of the diamond created.
// * Parameter List: N/A
// * Author: Juan Jose Ospina
// * Date: May 23, 2014
// **********************************************

void Diamond::Summary()
{
	cout << "The diamond size is : " << sizeofdiamond << " units"<< endl;
	cout << "The perimeter is : " << Perimeter() << " units"<< endl;
	cout << "The area is : " << fixed << setprecision(2) << Area()
		<< " units" << endl;

	Draw();
}
