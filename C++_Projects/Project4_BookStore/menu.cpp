/**********************************************************/
// Project:  Menu Book Inventory
// Author:   Juan Jose Ospina Casas
// Email:    jjo11e@my.fsu.edu
// Course :  COP3330
// Project:  4
// Due Date: 06/26/2014
// Summary:  The program creates a Store, where there is
// 	     an inventory of Books and cash in the register.
// 	     This creates the user interface.
// Output:   Creates the user interface.
// Assumptions:	Title must be 30 chars max. 
// 		Author must be 20 chars max.
/**********************************************************/

#include <iostream>
#include <iomanip>
#include "store.h"

using namespace std;

// ------------------------------------------------------
// Prototypes*
// ------------------------------------------------------

void SetBook(Store& s);		// Adds a Book.
void Lookup(Store& s1);		// Search for a Book or Books.
void Remove(Store& s1);		// Sell a Book. (Remove)
void GenreLook(Store& s1);	// Search Books by Genre.
void ShowMenu();		// Shows Menu.
char GetCmd();			// Gets the command form char entered.
char GetAChar(const char* ask); // Gets char from keyboard.
char Legal(char c);		// Checks if char/command is Legal.

// ------------------------------------------------------
// Functions*
// ------------------------------------------------------

// *****************************************
// * Function : main
// * Description: Performs the creation of
// 		  the interface.
// * Author: Juan Jose Ospina
// * Date: June 24, 2014
// *****************************************

int main()
{
	double startCash;		// Start cash of the register.

	cout<<"*** Welcome to Bookstore Inventory Manager ***\n";
	cout<<"Please Input the starting money in the cash register: "; 
	cin >> startCash;

	Store s1; 			// Creates and Initializes a Store.
	s1.SetCashReg(startCash);	// Sets starting cash.
	ShowMenu();			// Display Menu
	char command;		    	// Char of command received.

	do
	{
		command = GetCmd();

		switch(command)
		{
			case 'A': 
				SetBook(s1);
				break;
			case 'F': 
				Lookup(s1);
				break;
			case 'S': 
				Remove(s1);
				break;
			case 'D': 
				s1.DisplayInventory();
				break;
			case 'G': 
				GenreLook(s1);	
				break;
			case 'M': 
				ShowMenu();
				break;
			case 'X':
				cout<< "\nCash Register: $ "
				    << fixed << setprecision(2)
				    << s1.GetCashReg() << "\n"
				    << "GoodBye!\n\n"; 
				break;		
		}
	}while(command != 'X');
}


// ------------------------------------------------------
// Interface Functions*
// ------------------------------------------------------

// *****************************************
// * Function : ShowMenu
// * Description: Shows the Menu. User Interface.
// * Author: Juan Jose Ospina
// * Date: June 24, 2014
// *****************************************

void ShowMenu()
{
	cout << "\n\t\t*** BOOKSTORE MENU ***";
	cout << "\n\tA \tAdd a Book to Inventory";
	cout << "\n\tF \tFind a Book from Inventory";
	cout << "\n\tS \tSell a Book";
	cout << "\n\tD \tDisplay the inventory list";
	cout << "\n\tG \tGenre Summary";
	cout << "\n\tM \tShow this Menu";
	cout << "\n\tX \tExit";
}

// *****************************************
// * Function : GetCmd
// * Description: Ask for a char/command until
// 		  a legal one is entered. Returns
// 		  the char.
// * Author: Juan Jose Ospina
// * Date: June 24, 2014
// *****************************************

char GetCmd()
{
	char cmd = GetAChar("\n\n> ");	// Get a command character.

	while (!Legal(cmd))		
		{				
			cout << "\nIllegal command, Try again.\n";
			cmd = GetAChar("\n\n>");
		}
	return cmd;
}

// *****************************************
// * Function : GetAChar 
// * Description: Ask user for a single char,
// 		  and discards the return char.
// * Parameter List:
//   const char* ask - Prompt c-string.
// * Author: Juan Jose Ospina
// * Date: June 24, 2014
// *****************************************

char GetAChar(const char* ask)
{
	char option;			// Char to be returned.

	cout << ask;			// Prompt the user.
	cin >> option;			// Gets a char.
	option = toupper(option);	// Convert it to uppercase.
	cin.get();			// Discard newline char from input.

	return option;
}


// *****************************************
// * Function : Legal
// * Description: Checks the validity of the
// 		  char entered.
// * Parameter List: char c - char entered.
// * Author: Juan Jose Ospina
// * Date: June 24, 2014
// *****************************************

char Legal(char c)
{
	return	((c == 'A') || (c == 'F') || (c == 'S') || (c == 'D') ||
			(c == 'G') || (c == 'M') || (c == 'X'));
}

// ------------------------------------------------------
// *Accesors Functions
// ------------------------------------------------------


// *****************************************
// * Function : SetBook
// * Description: Adds a Book to the inventory
// 		  list on the Store.
// * Parameter List: Store& s - Store object.
// * Author: Juan Jose Ospina
// * Date: June 24, 2014
// *****************************************

void SetBook(Store& s)
{
	char titleIn[31];  	  // Stores Title
	char authorIn[21]; 	  // Stores Author.
	char tempType;	          // Stores temp char given by user.
	Genre typeIn;	     	  // Stores Genre clarification.
	double priceIn;	      	  // Stores the proce
	bool checkingGen;         // Checks if char given is correct.

	cout << "Enter title: " ;
	cin.getline(titleIn,31);

	cout << "Enter author: ";
	cin.getline(authorIn,21);

	do
	{
		cout<<"Enter Genre-(F)iction,(M)ystery,(S)ci-Fi,(C)omputer: ";
		cin >> tempType;

		switch(tempType)
		{

			case 'f':case 'F':
				typeIn = FICTION;
				checkingGen = true;
				break;
			case 'm':case 'M':
				typeIn = MYSTERY;
				checkingGen = true;
				break;
			case 's':case 'S':
				typeIn = SCIFI;
				checkingGen = true;
				break;
			case 'c':case 'C':
				typeIn = COMPUTER;
				checkingGen = true;
				break;
			default:
				cout << "Invalid Genre. Re-enter" << "\n";
				checkingGen = false;
				break;
		}

	}while(checkingGen==false);

	do
	{
		cout << "Enter Price(Non-negative): ";
		cin >> priceIn;

	}while(priceIn<0);

	cin.ignore();					// Flush the cin.

	s.AddBook(titleIn,authorIn,typeIn,priceIn);	// Sends Info Store.

}

// *****************************************
// * Function : Lookup
// * Description: Search a Book or Books by 
// 		  name or by author name.
// * Parameter List: Store& s1 - Store object.
// * Author: Juan Jose Ospina
// * Date: June 24, 2014
// *****************************************

void Lookup(Store& s1)
{
	char aName[31];

	cout << "Type a Book or Author to Search: ";
	cin.getline(aName,31);

	s1.LookInStore(aName);
}

// *****************************************
// * Function : Remove
// * Description: Sells a Book.
// * Parameter List: Store& s1 - Store object.
// * Author: Juan Jose Ospina
// * Date: June 24, 2014
// *****************************************

void Remove(Store& s1)
{

	char aName[31];

	cout << "Type a Book to Sell: ";
	cin.getline(aName,31);

	s1.Remove(aName);

}

// *****************************************
// * Function: GenreLook
// * Description: Search Books by genre.
// * Parameter List: Store& s1 - Store object.
// * Author: Juan Jose Ospina
// * Date: June 24, 2014
// *****************************************

void GenreLook(Store& s1)
{
	char optenter;
	Genre typeSearch;
	bool checkingGen;

	cout<<"Enter Genre-(F)iction,(M)ystery,(S)ci-Fi,(C)omputer: "; 

	do
	{
		cin >> optenter;		// Gets a char.
		optenter = toupper(optenter);	// Convert it to uppercase.
		cin.get();			// Discard newline.

		switch(optenter)
		{

			case 'F':
				typeSearch = FICTION;
				checkingGen = true;
				break;
			case 'M':
				typeSearch = MYSTERY;
				checkingGen = true;
				break;
			case 'S':
				typeSearch = SCIFI;
				checkingGen = true;
				break;
			case 'C':
				typeSearch = COMPUTER;
				checkingGen = true;
				break;
			default:
				cout << "Invalid Genre. Re-enter: " ;
				checkingGen = false;
				break;
		}

	}while(checkingGen==false);

	s1.SearchGenre(typeSearch);	
}
