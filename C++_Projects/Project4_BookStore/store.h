/*************************************************************/
// Project: Store Class Header
// Author:  Juan Jose Ospina Casas
// Email:   jjo11e@my.fsu.edu
// Course:  COP3330
// Project: 4
// Due Date:06/26/2014
// Summary:  Blueprint for a Store object.
//           It's represented by Book inventory and a 
//           money available in the cash register.
//           This class uses Book class as member data.
// Output:   Creates a blueprint for a Store object.
/*************************************************************/

#include "book.h"

class Store
{

	public:

		Store();		// Constructs an empty Store.
		~Store();		// Memory deallocation.

		void AddBook(const char* t, const char* a, Genre g, double p);
		void DisplayInventory() const;
		void LookInStore(char* aName)const;
		void Remove(char* aName);
		void SetCashReg(double c);
		void SearchGenre(Genre opt)const;
		double GetCashReg()const;

	private:

		// Member Data

		double cashReg; 	// Cash Register.
		int maxSize;		// Maximum current allowable.
		int currentSize; 	// Size of inventory (counting Var.)
		Book* inventList; 	// pointer to the inventory list.

		// Functions

		void Grow(); 			// Increase size.(Allocation)
		void Shrink(); 			// Decrease size.(Allocation)
		int FindBook(char*) const; 	// Finds index by Title.
		int FindByAuthor(char*, int) const;// Finds index by Author.
		int FindByGenre(Genre, int)const;  // Finds index by Genre.
};
