/**********************************************************/
// Project:  Store Class
// Author:   Juan Jose Ospina Casas
// Email:    jjo11e@my.fsu.edu
// Course :  COP3330
// Project:  4	
// Due Date: 06/26/2014
// Summary:  Blueprint for a Store object.
// 	     It's represented by Book inventory and a 
// 	     money available in the cash register.
// 	     This class uses Book class as member data.
// Output:   Creates a blueprint for a Store object.
/**********************************************************/

#include <iostream>
#include <cstring>
#include <iomanip>
#include "store.h"

using namespace std;

// *****************************************
// * Constructor: Store 
// * Description: Constructs empty Store.
// * Author: Juan Jose Ospina
// * Date: June 23, 2014
// *****************************************

Store::Store() 
{
	cashReg = 0.0;
	maxSize = 5;
	currentSize=0;
	inventList = new Book[maxSize];
}

// *****************************************
// * Destructor : ~Store 
// * Description: Handles the deallocation
// 		  of dynamic memory. Prevents
// 		  memory leaking.
// * Author: Juan Jose Ospina
// * Date: June 23, 2014
// *****************************************

Store::~Store() 
{
	delete [] inventList;
}

// *****************************************
// * Function : Grow
// * Description: Grows the Book dynamic array
// 		  by 5 slots. Handles new memory
// 		  allocation for the extra books.
// * Author: Juan Jose Ospina
// * Date: June 23, 2014
// *****************************************

void Store::Grow()
{

	maxSize = currentSize + 5;		// Expand to new size
	Book* tempList = new Book[maxSize];	// Allocate new Array

	for(int i=0; i<currentSize;i++)		// Copy each i into new
		tempList[i] = inventList[i];	// Array.

	delete [] inventList;			// Remove old Array.
	inventList = tempList;			// Point old name to new array

	cout << "\n** Array is being resized to: " << maxSize
		<< " Allocated slots\n" << endl;	
}

// *****************************************
// * Function : Shrink
// * Description: Shrinks the Book dynamic array
// 		  to the number of exact needed 
// 		  slots. 
// * Author: Juan Jose Ospina
// * Date: June 23, 2014
// *****************************************

void Store::Shrink()
{

	maxSize = currentSize;		// Expand to new size
	Book* tempList = new Book[maxSize];	// Allocate new Array

	for(int i=0; i<currentSize;i++)		// Copy each i into new
		tempList[i] = inventList[i];	// Array.

	delete [] inventList;			// Remove old Array.
	inventList = tempList;			// Point old name to new array

	cout << "\n** Array is being resized to: " << maxSize
		<< " Allocated slots\n" << endl;	
}

// *****************************************
// * Function : FindBook
// * Description: Search for a Book by title and 
// 		  returns the index if found, or
// 		  returns -1 if not found.
// * Parameter List: char* aBook - Title of Book
// 				   received.
// * Author: Juan Jose Ospina
// * Date: June 24, 2014
// *****************************************

int Store::FindBook(char* aBook) const
{
	for(int i=0;i<currentSize;i++)
		if(strcmp(inventList[i].GetTitle(),aBook)==0)
			return i; 

	return -1;		
}

// *****************************************
// * Function : FindByAuthor
// * Description: Search a Book by Author and
// 		  returns the index if found, or
// 		  returns -1 if not found.
// * Parameter List: 
//   char* aAuthor - Author's name.
//   int start     - indicates where to start the
//   		     searching procedure.
// * Author: Juan Jose Ospina
// * Date: June 24, 2014
// *****************************************

int Store::FindByAuthor(char* aAuthor, int start) const
{
	for(int i=start;i<currentSize;i++)
		if(strcmp(inventList[i].GetAuthor(),aAuthor)==0)
			return i; 

	return -1;		
}

// *****************************************
// * Function : AddBook
// * Description: Adds a Book object to the
// 		  inventory list. If inventory
// 		  is full it grows the inventory
// 		  by 5 slots.
// * Parameter List: 
//   const char* t - Title of the Book.
//   const char* a - Author of the Book.
//   Genre g       - Genre of the Book.
//   double p      - Price of the Book.
// * Author: Juan Jose Ospina
// * Date: June 24, 2014
// *****************************************

void Store::AddBook(const char* t, const char* a, Genre g, double p)
{
	if(currentSize == maxSize)  		// Inventory is full.
		Grow();

	inventList[currentSize++].Set(t,a,g,p); // Read new Book.
}

// *****************************************
// * Function : DisplayInventory
// * Description: Displays all the Books present
// 		  on the inventory. If there is
// 		  no Book, it will display that
// 		  the inventory is empty.
// * Author: Juan Jose Ospina
// * Date: June 24, 2014
// *****************************************

void Store::DisplayInventory() const
{

	if(currentSize==0)
	{	
		cout << "\nCurrent inventory is empty.\n";
	}else
	{
		cout<<left;
		cout <<"\n" <<setw(20)<<  "Title" << setw(20) 
			<< "Author" << setw(20) 
			<< "Genre"  << "Price" << endl << endl;

		for(int i=0; i<currentSize;i++)
			inventList[i].Display();


		cout<<"\nNo. of Books: "<< currentSize;
		cout<<"\nCash in Register: $ "<<fixed<<setprecision(2)<<cashReg;
	}
}

// *****************************************
// * Function : LookInStore
// * Description: Search for a Book or all
// 		  the Books by author and display
// 		  them. If the parameter given is 
// 		  not found it will display not 
// 		  found.
// * Parameter List: 
//   char* aName - char array of the name received.
// * Author: Juan Jose Ospina
// * Date: June 24, 2014
// *****************************************

void Store::LookInStore(char* aName)const
{

	int neededBook = FindBook(aName);
	int neededAuthor = FindByAuthor(aName,0);

	if(neededBook ==-1 && neededAuthor == -1)
		cout <<"\n"<< aName << " Book/Author Not Found in Store!\n";


	else if(neededBook == -1)  // Search By Author
	{

		cout << "\nBooks Found: \n\n";

		do	
		{
			inventList[neededAuthor].Display();
			neededAuthor++;
			neededAuthor = FindByAuthor(aName,neededAuthor);

		}while(neededAuthor != -1);

	}

	else 			  // Search By Book
	{
		cout << "\nBook Found: \n\n";
		inventList[neededBook].Display();
	}

}

// *****************************************
// * Function : Remove
// * Description: Removes a Book from the 
// 		  inventory list. If the number
// 		  of available slots is greater
// 		  than the slots used plus  5,
// 		  it will shrink the array to
// 		  hold just the require slots. 
// * Parameter List: 
//   char* aName - char array of name received.
// * Author: Juan Jose Ospina
// * Date: June 24, 2014
// *****************************************

void Store::Remove(char* aName)
{

	int removingBook = FindBook(aName);

	if(removingBook == -1)
	{
		cout << "\n" << aName << " Book Not Found in Store!\n";
		cout << "* No Sale Made *\n";
	}	
	else
	{
		// Shift each element "down", deleting the desire entry.
		cashReg = inventList[removingBook].GetPrice() + cashReg;

		for(int j=removingBook+1; j < currentSize; j++)
			inventList[j-1] = inventList[j];

		currentSize--;	// Decrement current number of books.

		cout << "\nBook Sold!\n ";


		// Shrinks the size of the array.
		if(maxSize > currentSize+5)
			Shrink();
	}

}

// *****************************************
// * Function : SetCashReg
// * Description: Sets the number/money for
// 		  the cash register of the Store.
// * Parameter List: double c - cash received.
// * Author: Juan Jose Ospina
// * Date: June 24, 2014
// *****************************************

void Store::SetCashReg(double c)
{
	cashReg = c;
}

// *****************************************
// * Function : GetCashReg
// * Description: Get the cash in the register.
// * Author: Juan Jose Ospina
// * Date: June 24, 2014
// *****************************************

double Store::GetCashReg()const
{
	return cashReg;
}


// *****************************************
// * Function : FindByGenre
// * Description: Finds a Book by Genre. 
// 		  If found returns index.
// 		  If not found returns -1.
// * Parameter List: 
//   Genre opt  - Genre to look.
//   int start  - where to star the searching
//   		  index.
// * Author: Juan Jose Ospina
// * Date: June 24, 2014
// *****************************************

int Store::FindByGenre(Genre opt, int start)const
{

	for(int i=start;i<currentSize;i++)
		if(inventList[i].GetGenre()==opt)
			return i; // if found, return index and exit.

	return -1;		// Retrun -1 if never found.

}

// *****************************************
// * Function : SearchGenre
// * Description: Search a Book by genre and
// 		  displays all the books of the
// 		  specific genre only, the total
// 		  number of Books, and the sum of 
// 		  the Books prices.
// * Parameter List: Genre opt - Genre to search.
// * Author: Juan Jose Ospina
// * Date: June 24, 2014
// *****************************************

void Store::SearchGenre(Genre opt)const
{
	int neededGenre = FindByGenre(opt,0);
	int counterGen = 0;			// Counts number of Books.
	double totalGen = 0.0;			// Sum of Books prices.

	if (neededGenre == -1)
	{
		cout << "\nTotal Books of Genre: " << counterGen;
		cout << "\nTotal Cost of Books: $ " << fixed
			<< setprecision(2) << totalGen;

	}	
	else
	{
		cout << "\nBooks Found by Genre: \n\n";

		do	
		{
			inventList[neededGenre].Display();
			totalGen = inventList[neededGenre].GetPrice()+totalGen;		
			neededGenre++;
			counterGen++;
			neededGenre = FindByGenre(opt,neededGenre);

		}while(neededGenre != -1);

		cout << "\nTotal Books of Genre: " << counterGen;
		cout << "\nTotal Cost of Books: $ " << fixed 
			<< setprecision(2) << totalGen;
	}

}

