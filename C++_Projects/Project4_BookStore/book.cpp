/**********************************************************/
// Project:  Book Class
// Author:   Juan Jose Ospina Casas
// Email:    jjo11e@my.fsu.edu
// Course :  COP3330
// Project:  4	
// Due Date: 06/26/2014
// Summary:  This class is the blueprint for a Book object.
// 	     It's represented with a title, author, Genre
// 	     type and price.
// Output:   Creates a Book blueprint for creating
// 	     Book objects that can be used in many
// 	     applications.
// Assumptions:	Books must have a title, author, genre,
// 		and a price.
/**********************************************************/

#include <iostream>
#include <cstring>
#include <iomanip>
#include "book.h"

using namespace std;


// *****************************************
// * Constructor: Book 
// * Description: Constructs blank Book.
// * Author: Juan Jose Ospina
// * Date: June 22, 2014
// *****************************************

Book::Book() 
{
	strcpy(title," ");
	strcpy(author," ");
	price = 0.0;
	type = FICTION; 
}

// *****************************************
// * Function : Set
// * Description: Sets the Book object with 
// 		  the parameters passed in. 
// * Parameter List: 
//   const char* t - title char array.
//   const char* a - author char array.
//   Genre g - genre type.
//   double p - price of book.
// * Author: Juan Jose Ospina
// * Date: June 22, 2014
// *****************************************


void Book::Set(const char* t, const char* a, Genre g, double p)
{

	// SEE IF ATER YOU CAN APPLY CHECKING WITH LENGTH IN EACH STRCPY
//	int tlength = strlen(t); // Get the length to do safer string oper.
//	int alength= strlen(a);

	//strncpy(title,t,tlength);
	//strncpy(author,a,alength);

	strcpy(title, t);
	strcpy(author, a);

	type = g;
	price = p;

}

// *****************************************
// * Function : GetTitle
// * Description: returns the Title of the 
// 		  Book object.
// * Author: Juan Jose Ospina
// * Date: June 22, 2014
// *****************************************

const char* Book::GetTitle() const
{
	return title;
}

// *****************************************
// * Function : GetAuthor
// * Description: returns the Author of the
// 		  Book object.
// * Author: Juan Jose Ospina
// * Date: June 22, 2014
// *****************************************

const char* Book::GetAuthor() const
{
	return author;
}

// *****************************************
// * Function : GetPrice
// * Description: returns the Price of the 
// 		  Book object.
// * Author: Juan Jose Ospina
// * Date: June 22, 2014
// *****************************************

double Book::GetPrice() const
{
	return price;
}

// *****************************************
// * Function : GetGenre
// * Description: returns the Genre of the
// 		  Book object.
// * Author: Juan Jose Ospina
// * Date: June 22, 2014
// *****************************************

Genre Book::GetGenre() const
{
	return type;
}

// *****************************************
// * Function : Display 
// * Description: Displays/prints the Book
// 		  object on one line.
// * Author: Juan Jose Ospina
// * Date: June 22, 2014
// *****************************************

void Book::Display() const
{

	cout<<left;

	//Book Info.

	cout  << setw(20)<< GetTitle() << setw(20) 
		<< GetAuthor() << setw(20);

	switch(type)
	{	
		case FICTION:
			cout << "Fiction";
			break;
		case MYSTERY:
			cout << "Mystery";
			break;
		case SCIFI:
			cout << "Sci-Fi";
			break;
		case COMPUTER:
			cout << "Computer";
			break;
	}

	cout  <<"$ "<< fixed <<  setprecision(2) << price << "\n";

}


