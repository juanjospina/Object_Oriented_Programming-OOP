/**********************************************************/
// Project:  Mixed Number Class
// Author:   Juan Jose Ospina Casas
// Email:    jjo11e@my.fsu.edu
// Course :  COP3330
// Project:  3	
// Due Date: 06/11/2014
// Summary:  The program creates a Mixed number object
// 	     that is represented as a whole numerator/
// 	     denominator. 
// Output:   Creates a Mixed number blueprint for creating
// 	     mixed number objects that can be used in many
// 	     applications.
// Assumptions:	User must enter Mixed numbers as: 1 3/4
/**********************************************************/

#include <iostream>
#include "mixed.h" 


using namespace std;

//*****************************************
// Friend Functions -----------------------
// **************************************** 

// *****************************************
// * Friend Function: operator>> 
// * Description: Extraction operator overload. 
// * Parameter List: istream& inmix - istream
// 		     Mixed& m - Mixed object
// * Author: Juan Jose Ospina
// * Date: June 09, 2014
// *****************************************

istream& operator>>(istream& inmix, Mixed& m)
{
	char line;

	inmix >> m.entero  >> m.numerator 
		>> line >> m.denominator;

	if((m.denominator <=0) )	
	{	
		m.entero=0;
		m.numerator=0;
		m.denominator=1;
	}else
	{
		if(m.entero!=0)
		{
			if(m.numerator<0)
			{
				m.entero=0;
				m.numerator=0;
				m.denominator=1;
			}
		}
	}

	return inmix;
}


// *****************************************
// * Friend Function: operator<< 
// * Description: Insertion operator overload. 
// * Parameter List: ostream& outmix - ostream
// 		     const Mixed& m - Mixed object
// * Author: Juan Jose Ospina
// * Date: June 09, 2014
// *****************************************

ostream& operator<<(ostream& outmix, const Mixed& m)
{

	if (m.entero==0 && m.numerator==0)
		outmix << m.entero;
	else
	{
		if(m.entero==0)
		{	
			if(m.denominator<0)
				outmix<<m.numerator<<'/'<<(m.denominator*-1);
			else
				outmix<<m.numerator<<'/'<<m.denominator;
		}
		else
		{
			if(m.numerator==0)
				outmix << m.entero;
			else
			{
				if(m.entero<0 && m.numerator<0)
					outmix << m.entero<<' '
					<<m.numerator*-1
				        <<'/'<<m.denominator;

				else if(m.entero<0 && m.denominator<0)
					outmix << m.entero<<' '
					<<m.numerator
				        <<'/'<<m.denominator*-1;
				else if(m.numerator<0 && m.denominator<0)
					outmix << m.entero<<' '
					<<m.numerator
				        <<'/'<<m.denominator*-1;
				else
					outmix << m.entero<<' '
					<<m.numerator
				        <<'/'<<m.denominator;
			}
		}
	}	

	return outmix; 

}

//*****************************************
// Member Functions -----------------------
// **************************************** 


// *****************************************
// * Constructor: Mixed 
// * Description: Constructs Mixed object
// * Parameter List: int e - whole number
// 		     int num - numerator
// 		     int den - denominator
// * Author: Juan Jose Ospina
// * Date: June 09, 2014
// *****************************************

Mixed::Mixed(int e, int num, int den)
{
	CheckValid(e,num,den);
}

// *****************************************
// * Conversion Constructor: Mixed 
// * Description: Constructs Mixed object
// * Parameter List: int e - whole number
// * Author: Juan Jose Ospina
// * Date: June 09, 2014
// *****************************************

Mixed::Mixed(int ent)
{

	entero = ent;
	numerator = 0;
	denominator = 1;

}

// *****************************************
// * Function : CheckValid
// * Description: Checks the validity of Mixed
// 		  object's member data. Returns
// 		  true if valid, false otherwise.
// * Parameter List: int e - whole
// 		     int num - numerator
// 		     int den - denominator
// * Author: Juan Jose Ospina
// * Date: June 09, 2014
// *****************************************

bool Mixed::CheckValid(int e, int num, int den)
{

	if((den <=0))			
	{	
		entero=0;
		numerator=0;
		denominator=1;
		return false;
	}else
	{
		if(e==0)			// Mixed : 0 +-3/4
		{
			entero=e;
			numerator=num;
			denominator=den;
			return true;

		}else
		{
			if(num<=0)		// Mixed: 5 -3/4
			{
				entero=0;
				numerator=0;
				denominator=1;
				return false;
			}else
			{
				entero=e;
				numerator=num;
				denominator=den;
				return true;
			}
		}

	}
}

// *****************************************
// * Function: Evaluate 
// * Description: Evaluates the Mixed number.
// 		  Returns decimal value.
// * Author: Juan Jose Ospina
// * Date: June 09, 2014
// *****************************************

double Mixed::Evaluate() const
{
	double fracnum = numerator;
	double fracdenom = denominator;
	double tempentero = entero;
	double tempresult;

	if(tempentero<0)
	{
		tempentero = tempentero*-1;
		tempresult = ((fracnum/fracdenom)+tempentero)*-1;	
	}else
		tempresult = (fracnum/fracdenom)+tempentero;

	return tempresult;
}

// *****************************************
// * Function: Simplify 
// * Description: Simplifies the Mixed number
// 		  to the lowest terms.
// * Author: Juan Jose Ospina
// * Date: June 09, 2014
// *****************************************

void Mixed::Simplify()
{

	int whole;                 // stores the integer division
	int numposit; 		   // stores the abs value of numerator
	int gcd = EucAlgorith();   // stores the GCD of the Mixed number.

	/* Getting the abs value of the numerator
	   in order to apply simplification formula 
	   2 cases improper and Proper*/

	if(numerator<0)
		numposit = numerator*-1;
	else
		numposit = numerator;


	// Mixed in Fraction form. 0 19/4
	if(entero==0)   
	{
		numerator = numerator/gcd;
		denominator = denominator/gcd;

		// Converts Fraction to Mix number.
		ToMixed(numposit);

		// Converts the fraction into a Mixed.
		if(denominator==1) 
		{
			entero = numerator;
			numerator = 0;
		}
	}

	// Normal Mixed Number
	else	
	{	
		// Case 1 - Improper Fraction
		if(numposit>denominator)
		{
			whole=numposit/denominator;       	

			if(entero<0)
			{
				entero=(entero*-1)+whole; 	
				numerator=numposit-(whole*denominator);
				entero = entero*-1;
			}
			else
			{
				entero=entero+whole;
				numerator=numposit-(whole*denominator);
			}

			numerator = numerator/gcd;
			denominator = denominator/gcd;
		}
		// Case 2 -Proper Fraction
		else	
		{	
			numerator = numerator/gcd;
			denominator = denominator/gcd;
		}
	}
}

// *****************************************
// * Function: EucAlgorith
// * Description: Calculates the GCD(Greatest
// 		   common denominator) using
// 		   the Euclidian Algorithm
// 		   proposition.
// * Author: Juan Jose Ospina
// * Date: June 09, 2014
// *****************************************

int Mixed::EucAlgorith()
{
	int gcd; 			// Greater common denominator.
	int remain = numerator;		// remainder.
	int dentemp = denominator;	// new-temp denominator.
	int numtemp = numerator;	// new-temp numerator.

	while(remain != 0)
	{
		remain = dentemp % numtemp;

		dentemp = numtemp;
		gcd = numtemp;
		numtemp = remain;
	}

	if (gcd<0)
		gcd = gcd *-1;

	return gcd;

}

// *****************************************
// * Function: ToFraction  
// * Description: Converts the Mixed number
// 		  to a Fraction.
// * Author: Juan Jose Ospina
// * Date: June 10, 2014
// *****************************************

//
//
// FALLAA CON LOS NEGATIVE
// MIXED NUMBERS CON IMPROPER FRACTIONS
//

void Mixed::ToFraction()
{
	int entposit;

	if(entero<0)
	{
		entposit = (entero)*-1;
		numerator =((denominator*entposit) + numerator)*-1;
		entero=0;
	}else
	{
		entposit = entero;
		numerator =(denominator*entposit + numerator);
		entero=0;
	}

}

// *****************************************
// * Function: ToMixed() 
// * Description: Converts a Fraction(Mixed)
// 		  into a Mixed number. 
// * Author: Juan Jose Ospina
// * Date: June 10, 2014
// *****************************************
void Mixed::ToMixed()
{
	int whole;
	int remain;

	if(numerator>denominator)
	{
		remain = numerator%denominator;
		whole = numerator/denominator;	

		numerator = remain;
		entero = whole;
	}
}

// *****************************************
// * Function: ToMixed(const int& n)
// * Description: Converts the Fraction
// 		 into a Mixed number. 
// * Author: Juan Jose Ospina
// * Date: June 10, 2014
// *****************************************

void Mixed::ToMixed(const int& n)
{

	int whole;
	int remain;

	if(n>denominator)
	{
		remain = numerator%denominator;
		whole = numerator/denominator;	

		if (numerator<0)
		{
			entero = whole;
			numerator = remain*-1;
		}
		else
			entero = whole;
	}
}



// *****************************************
// * Function: operator< 
// * Description: Less than operator overload. 
// * Parameter List: const Mixed& b - second
// 		     object used to compare 
// 		     with calling object.
// * Author: Juan Jose Ospina
// * Date: June 10, 2014
// *****************************************

bool Mixed::operator<(const Mixed& b)const
{

	int crossa;
	int crossb;

	Mixed tempa;
	Mixed tempb;	

	tempa.numerator = (denominator*entero) + numerator;
	tempa.denominator = denominator;
	tempa.entero=0;

	tempb.numerator = (b.denominator*b.entero) + b.numerator;
	tempb.denominator = b.denominator;
	tempb.entero=0;

	// Cross Multiply in order to compare.
	crossa = tempa.numerator * tempb.denominator;
	crossb = tempa.denominator * tempb.numerator;	

	if(crossa<crossb)
		return true;
	else
		return false;
}

// *****************************************
// * Function: operator> 
// * Description: Greater than operator overload. 
// * Parameter List: const Mixed& b - second
// 		     object used to compare 
// 		     with calling object.
// * Author: Juan Jose Ospina
// * Date: June 10, 2014
// *****************************************

bool Mixed::operator>(const Mixed& b) const
{
	return b<*this;
}

// *****************************************
// * Function: operator<= 
// * Description: Less than or equal to
// 		  operator overload. 
// * Parameter List: const Mixed& b - second
// 		     object used to compare 
// 		     with calling object.
// * Author: Juan Jose Ospina
// * Date: June 10, 2014
// *****************************************

bool Mixed::operator<=(const Mixed& b) const
{
	return !(b<*this);
}

// *****************************************
// * Function: operator>= 
// * Description: Greater than or equal to
// 		  operator overload. 
// * Parameter List: const Mixed& b - second
// 		     object used to compare 
// 		     with calling object.
// * Author: Juan Jose Ospina
// * Date: June 10, 2014
// *****************************************

bool Mixed::operator>=(const Mixed& b) const
{
	return !(*this<b);
}

// *****************************************
// * Function: operator== 
// * Description: Equal to operator overload. 
// * Parameter List: const Mixed& b - second
// 		     object used to compare 
// 		     with calling object.
// * Author: Juan Jose Ospina
// * Date: June 10, 2014
// *****************************************

bool Mixed::operator==(const Mixed& b) const
{
	return (!(*this<b) && !(b<*this));
}

// *****************************************
// * Function: operator!= 
// * Description: Not Equal to operator overload. 
// * Parameter List: const Mixed& b - second
// 		     object used to compare 
// 		     with calling object.
// * Author: Juan Jose Ospina
// * Date: June 10, 2014
// *****************************************

bool Mixed::operator!=(const Mixed& b) const
{
	return ((*this<b) || (b<*this));
}

// *****************************************
// * Function:operator+ 
// * Description: Adding infix overload.
// * Parameter List: const Mixed& m
// * Author: Juan Jose Ospina
// * Date: June 10, 2014
// *****************************************

Mixed Mixed::operator+(const Mixed& m)const
{
	Mixed result;			 // store the result for the addition.

	Mixed csimpl = *this; 		// copy of calling object.
	Mixed msimpl = m;     		// copy of second object.

	if(csimpl != 0)
		csimpl.Simplify();	// Simplify the calling object.
	if(msimpl !=0)
		msimpl.Simplify();	// Simplify de second object.


	csimpl.ToFraction();		// Converts to Fraction.
	msimpl.ToFraction();

	result.denominator = csimpl.denominator*msimpl.denominator; 
	result.numerator   = (csimpl.numerator*msimpl.denominator)+
		(csimpl.denominator*msimpl.numerator);

	result.ToMixed(); 		// Pass to Mixed Number.

	if(result !=0)	
		result.Simplify(); 	// Simplification of the fraction.

	return result;
}

// *****************************************
// * Function:operator- 
// * Description: Substract infix overload.
// * Parameter List: const Mixed& m
// * Author: Juan Jose Ospina
// * Date: June 10, 2014
// *****************************************

Mixed Mixed::operator-(const Mixed& m)const
{

	Mixed result; 		

	Mixed csimpl = *this; 
	Mixed msimpl = m;    

	if(csimpl != 0)
		csimpl.Simplify();	
	if(msimpl !=0)
		msimpl.Simplify();

	csimpl.ToFraction();
	msimpl.ToFraction();

	result.denominator= csimpl.denominator*msimpl.denominator; 
	result.numerator = ((csimpl.numerator*msimpl.denominator)-
			(csimpl.denominator*msimpl.numerator));

	result.ToMixed(); 

	if(result !=0)
		result.Simplify();

	return result;
}

// *****************************************
// * Function:operator* 
// * Description: Multiplication infix overload.
// * Parameter List: const Mixed& m
// * Author: Juan Jose Ospina
// * Date: June 10, 2014
// *****************************************
Mixed Mixed::operator*(const Mixed& m)const
{
	Mixed result; 

	Mixed csimpl = *this; 
	Mixed msimpl = m;    

	if(csimpl != 0)
		csimpl.Simplify();	
	if(msimpl !=0)
		msimpl.Simplify();

	csimpl.ToFraction();	
	msimpl.ToFraction();

	result.numerator = csimpl.numerator*msimpl.numerator;
	result.denominator= csimpl.denominator*msimpl.denominator;

	result.ToMixed();

	if(result !=0)
		result.Simplify(); 

	return result;
}

// *****************************************
// * Function:operator/ 
// * Description: Division infix overload.
// * Parameter List: const Mixed& m
// * Author: Juan Jose Ospina
// * Date: June 10, 2014
// *****************************************
Mixed Mixed::operator/(const Mixed& m)const
{

	Mixed result = 0;

	if(m==0)
		return result;
	else
	{
		Mixed csimpl = *this; 
		Mixed msimpl = m;     

		if(csimpl != 0)
			csimpl.Simplify();	
		if(msimpl !=0)
			msimpl.Simplify();	

		csimpl.ToFraction();	
		msimpl.ToFraction();

		result.numerator = csimpl.numerator*msimpl.denominator;
		result.denominator= csimpl.denominator*msimpl.numerator; 

		result.ToMixed(); 

		if(result !=0)
			result.Simplify(); 

		return result;
	}
}

// *****************************************
// * Function:operator++ 
// * Description: Pre-Increment
// * Author: Juan Jose Ospina
// * Date: June 10, 2014
// *****************************************

Mixed& Mixed::operator++()
{
	entero = entero+1;
	return *this;
}

// *****************************************
// * Function:operator++ 
// * Description: Post-Increment
// * Author: Juan Jose Ospina
// * Date: June 10, 2014
// *****************************************

Mixed Mixed::operator++(int)
{

	Mixed temp = *this;
	entero = entero+1;
	return temp;

}

// *****************************************
// * Function:operator-- 
// * Description: Pre-Decrement
// * Author: Juan Jose Ospina
// * Date: June 10, 2014
// *****************************************

Mixed& Mixed::operator--()
{
	entero = entero-1;
	return *this;
}

// *****************************************
// * Function:operator-- 
// * Description: Post-Decrement
// * Author: Juan Jose Ospina
// * Date: June 10, 2014
// *****************************************

Mixed Mixed::operator--(int)
{
	Mixed temp = *this;
	entero = entero-1;
	return temp;
}


