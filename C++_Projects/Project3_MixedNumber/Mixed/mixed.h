/*************************************************************/
// Project: Mixed Number Class Header
// Author:  Juan Jose Ospina Casas
// Email:   jjo11e@my.fsu.edu
// Course:  COP3330
// Project: 3
// Due Date:06/11/2014
// Summary:  The program creates a Mixed number object
//           that is represented as a whole numerator/
//           denominator. 
// Output:   Creates a Mixed number blueprint for creating
//           mixed number objects that can be used in many
//           applications.
// Assumptions: User must enter Mixed numbers as: 1 3/4
/*************************************************************/

#include <iostream>

using namespace std;


class Mixed
{
	// Friend Functions
	friend istream& operator>>(istream& inmix, Mixed& m);
	friend ostream& operator<<(ostream& outmix, const Mixed& m);


	public:

	Mixed(int,int,int);		     // Constructor
	Mixed(int ent = 0);		     // Conv. Constructor

	double Evaluate() const;	     // Evaluates decimal of number.
	void Simplify();		     // Simplifies the number.
	void ToFraction();		     // Converts Mixed to Fraction.

	bool operator<(const Mixed&)const;
	bool operator>(const Mixed&)const;
	bool operator<=(const Mixed&)const;
	bool operator>=(const Mixed&)const;
	bool operator==(const Mixed&)const;
	bool operator!=(const Mixed&)const;

	Mixed operator+(const Mixed& m)const;
	Mixed operator-(const Mixed& m)const;
	Mixed operator*(const Mixed& m)const;
	Mixed operator/(const Mixed& m)const;

	Mixed& operator++();
	Mixed operator++(int);
	Mixed& operator--();
	Mixed operator--(int);


	private:

	int entero; 			      // whole part. 
	int numerator; 			      // numerator of fraction part.
	int denominator; 		      // denominator of fraction part.

	int EucAlgorith();     		      // Euclids Algortihm(GDC)
	bool CheckValid(int,int,int);	      // Check valid input
	void ToMixed();                       // Converts Fraction to Mixed
	void ToMixed(const int&);	     /* Converts Fraction to Mixed
					     with the numerator as parameter*/

};
