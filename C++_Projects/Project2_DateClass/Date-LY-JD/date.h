/*************************************************************/
// Project: Date Class Header
// Author:  Juan Jose Ospina Casas
// Email:   jjo11e@my.fsu.edu
// Course:  COP3330
// Project: 2
// Due Date:06/02/2014
// Summary: The program creates any Date specified by the 
//  	    user. If the Date is invalid it doesn't change 
//  	    the state of the object. It takes into account
//  	    leap years, and also offers many format styles.  
// Output: Creates a Date blueprint for creating objects
// 	   of type Date that can be used in many applications.
// Assumptions:N/A
/*************************************************************/

#include <iostream>


using namespace std;

class Date
{

	public:
		
		// Constructor
		Date(int m=1, int =1, int y=2000); 

		// Accessors
		int GetMonth() const;
		int GetDay() const;     
		int GetYear() const;

		// Standard I/O routines
		void Input();			  // Input date from keyboard
		void Show() const;		  // Display date on screen.
	
		// Mutators
		bool SetFormat(char f);		  // Sets format style.
		bool Set(int m, int d, int y);	  // Sets date's value.

		// Other Functions
		void Increment(int numDays=1);	  // Increments date's value.

		int Compare(const Date& d) const; // Compares two Fraction 
					          // objects and returns if
					          // they are equal or which 
					          // one comes first chrono-
					          // logically.


	private:

		// Member Data
		int day; 			  
		int month;
		int year;
		char formatting;		 // format character selector.

		// Private Functions
		bool ValidDate(int m, int d, int y); // Validates the date 
						     // passed as parameters, 
						     // and if valid assigns
						     // them to member data.

		int JulDay() const;		     // Converts normal day
						     // to Julian day format.

};
