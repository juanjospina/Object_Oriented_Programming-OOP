/**********************************************************/
// Project:  Date Class
// Author:   Juan Jose Ospina Casas
// Email:    jjo11e@my.fsu.edu
// Course :  COP3330
// Project:  2	
// Due Date: 06/02/2014
// Summary:  The program creates any Date specified by the 
// 	     user. If the Date is invalid it doesn't change 
// 	     the state of the object. It takes into account
// 	     leap years, and also offers many format styles. 
// Output:   Creates a Date blueprint for creating objects
// 	     of type Date that can be used in many applications.
// Assumptions:	User must enter Dates as numbers. Ex. 1/2/1990
/**********************************************************/

#include <iostream>
#include <string>
#include "date.h"

using namespace std;

// *****************************************
// * Member Constructor: Date
// * Description: Constructs Date object
// * Parameter List: int m - month
// 		     int d - day
// 		     int y - year
// * Author: Juan Jose Ospina
// * Date: May 30, 2014
// *****************************************

Date::Date(int m, int d, int y)
{
	if(ValidDate(m,d,y)==false) // If invalid input,initializes to default.
	{
		day = 1;
		month = 1;
		year = 2000;

	}else
		ValidDate(m,d,y);
}

// *****************************************
// * Member Function: ValidDate
// * Description: Validates month, day, and
// 		  year given. If it's invalid
// 		  returns false. If conditions 
// 		  are fullfilled, it assigns 
// 		  parameters' values to member
// 		  data.  
// * Parameter List: int m - month
// 		     int d - day
// 		     int y - year
// * Author: Juan Jose Ospina
// * Date: May 30, 2014
// *****************************************

bool Date::ValidDate(int m, int d, int y)
{
	switch(m)
	{
		// Select the specific month depending on parameter.

		case 1:case 3:case 5:case 7:case 8:case 10:case 12:

			if((d>0 && d<= 31) && (m>0 && m<=12) && (y>0))
			{
				day = d;      
				month = m;
				year = y;
				return true;
			}
			else
				return false;
			break;

		case 4:case 6:case 9:case 11:

			if((d>0 && d<= 30) && (m>0 && m<=12) && (y>0))
			{
				day = d;
				month = m;
				year = y;
				return true;
			}
			else
				return false;
			break;

		case 2:
			if((y%4==0 && y%100!=0)||(y%400==0)) //Leap year check
			{
				if((d>0 && d<= 29) && (m>0 && m<=12) && (y>0))
				{
					day = d;
					month = m;
					year = y;
					return true;	
				}	
				else
					return false;
				break;

			}else
			{
				if((d>0 && d<= 28) && (m>0 && m<=12) && (y>0))
				{
					day = d;
					month = m;
					year = y;
					return true;	
				}	
				else
					return false;
				break;
			}

		default:
			return false;
			break;
	}

}

// *****************************************
// * Member Function: GetMonth
// * Description: Gets the month.
// * Parameter List: N/A
// * Author: Juan Jose Ospina
// * Date: May 30, 2014
// *****************************************

int Date::GetMonth() const
{
	return month;
}

// *****************************************
// * Member Function: GetDay
// * Description: Gets the day.
// * Parameter List: N/A
// * Author: Juan Jose Ospina
// * Date: May 30, 2014
// *****************************************

int Date::GetDay() const
{
	return day;
}

// *****************************************
// * Member Function: GetYear
// * Description: Gets the year.
// * Parameter List: N/A
// * Author: Juan Jose Ospina
// * Date: May 30, 2014
// *****************************************

int Date::GetYear() const
{
	return year;
}

// *****************************************
// * Member Function:Input 
// * Description: Prompts the user to input
// 	          a Date. If invalid input,
// 	          it prompts the user until
// 	          a valid Date is entered.
// 	          If valid it changes the 
// 	          calling object.  
// * Parameter List: N/A
// * Author: Juan Jose Ospina
// * Date: May 30, 2014
// *****************************************

void Date::Input()
{

	int a,b,c;   		// User entered month/day/year. 
	char delim;		// Char used to store '/'.

	do
	{
		cout<<"Please enter a Valid Date: ";
		cin >> a >> delim >> b >> delim >> c;
		ValidDate(a,b,c);

	}while(ValidDate(a,b,c)==false);
}

// *****************************************
// * Member Function: Show
// * Description: Prints out the Date in the
// 		  specified format.
// * Parameter List: N/A
// * Author: Juan Jose Ospina
// * Date: May 31, 2014
// *****************************************

void Date::Show() const
{

	string monthName[13] = {"nothing","Jan","Feb","Mar","Apr","May",
				"June","July","Aug","Sept","Oct","Nov","Dec"};

	switch(formatting)
	{

		case 'T':                      // Two-Digit: mm/dd/yy
			cout.fill('0');
			cout.width(2);
			cout<<month<<"/";

			cout.fill('0');
			cout.width(2);

			cout<<day<<"/";

			cout.fill('0');
			cout.width(2);
			cout<<year%100;
			break;

		case 'L':			// Long: Month Day, Year

			cout<<monthName[month]<<" " << day<<", "<<year;
			break;

		case 'J':			// Julian: YY-JJJ

			cout.fill('0');
			cout.width(2);
			cout<<year%100<<"-";
			cout.fill('0');
			cout.width(3);
			cout<<JulDay();
			break;

		case 'D': default:              // Default: M/D/Y

			cout << month << "/"<<day<<"/"<<year;
			break;
	}

}

// *****************************************
// * Member Function: SetFormat
// * Description: Sets the format according
// 		  to the code passed as
// 		  parameter by the user.
// 		  Returns true if format was
// 		  changed, false otherwise. 
// * Parameter List: char f - formatting char.
// * Author: Juan Jose Ospina
// * Date: May 30, 2014
// *****************************************


bool Date::SetFormat(char f)
{
	if(f == 'T' || f == 'L' || f == 'D' || f == 'J')
	{
		formatting = f;
		return true;
	}else
		return false;
}

// *****************************************
// * Member Function: Set 
// * Description: Sets the Date to the date
// 		  specified by the user input.
// 		  It calls ValidDate(int,int,int)
// 		  to check if date entered is 
// 		  valid. If valid, it changes the
// 		  Date. 
// * Parameter List: int m - month
// 		     int d - day
// 		     int y - year
// * Author: Juan Jose Ospina
// * Date: May 30,2014
// *****************************************

bool Date::Set(int m, int d, int y)
{
	if(ValidDate(m,d,y)==true)
		return true;
	else
		return false;
}

// *****************************************
// * Member Function: Increment
// * Description: Increments the Date according
// 		  to the parameter given. 
// * Parameter List: int numDays - number of
// 		    		   days to 
// 		    		   increment.
// * Author: Juan Jose Ospina
// * Date: May 31, 2014
// *****************************************


void Date::Increment(int numDays)
{

	for (int i=0;i<numDays;i++)
	{
		if(ValidDate(month,day,year)==true)
		{
			day = day + 1;

			if(ValidDate(month,day,year)==false)
			{
				day=1;

				if(month == 12)
				{
					month = 1;
					day = 1;
					year++;

				}else
					month++;
			}
		}
	}
}

// *****************************************
// * Member Function: Compare 
// * Description: Compares two Date objects.
// 		  The first is the calling
// 		  object and the second is
// 		  received as parameter.
// 		  Returns -1 -> d1 comes first.
// 		  Returns  0 -> d1 equals d2.
// 		  Returns  1 -> d1 comes later.
// * Parameter List: const Date& d-Date object
// 				   used to compare
// 				   with calling object.
// 				   Passed by reference,
// 				   and const, not allowing
// 				   it to change.
// * Author: Juan Jose Ospina
// * Date: May 31, 2014
// *****************************************

int Date::Compare(const Date& d) const
{

	if(year == d.year && month == d.month && day == d.month)
		return 0;
	else
	{
		if(year<d.year)				// Comparing Years.
			return -1;
		else if(year>d.year)
			return 1;
		else
		{
			if(month<d.month)		// Comparing Months.
				return -1;
			else if(month>d.month)
				return 1;
			else
			{
				if(day<d.day)		// Comparing Days.
					return-1;
				else
					return 1;
			}
		}	
	}
}

// *****************************************
// * Member Function: JulDay
// * Description: Change a Gregorian day to
// 	          Julian Day format. It takes
// 	          into account leap years.
// 	          Returns the number day 
// 	          according to the date.
// 	          Ex. Feb 1 -> 032  
// * Parameter List: N/A
// * Author: Juan Jose Ospina
// * Date: May 31, 2014
// *****************************************


int Date::JulDay() const
{
	int initDay=0;			// Numbers of days before the date.

	switch(month)
	{
		case 2:
			initDay=31;
			break;	
		case 3:
			if((year%4==0 && year%100!=0)||(year%400==0)) // Leap.
				initDay=60;
			else
				initDay=59;
			break;
		case 4:
			if((year%4==0 && year%100!=0)||(year%400==0))	
				initDay=91;
			else
				initDay=90;
			break;
		case 5:
			if((year%4==0 && year%100!=0)||(year%400==0))	
				initDay=121;
			else
				initDay=120;
			break;
		case 6:
			if((year%4==0 && year%100!=0)||(year%400==0))	
				initDay=152;
			else
				initDay=151;
			break;
		case 7:
			if((year%4==0 && year%100!=0)||(year%400==0))	
				initDay=182;
			else
				initDay=181;
			break;
		case 8:
			if((year%4==0 && year%100!=0)||(year%400==0))	
				initDay=213;
			else
				initDay=212;
			break;
		case 9:
			if((year%4==0 && year%100!=0)||(year%400==0))	
				initDay=244;
			else
				initDay=243;
			break;
		case 10:
			if((year%4==0 && year%100!=0)||(year%400==0))	
				initDay=274;
			else
				initDay=273;
			break;
		case 11:
			if((year%4==0 && year%100!=0)||(year%400==0))	
				initDay=305;
			else
				initDay=304;
			break;
		case 12:
			if((year%4==0 && year%100!=0)||(year%400==0))	
				initDay=335;
			else
				initDay=334;
			break;
		default:
			initDay=0;
	}	

	return initDay+day;
} 
