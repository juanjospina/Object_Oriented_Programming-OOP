#include <iostream>


using namespace std;

class Date
{


	public:
		Date(int m=1, int =1, int y=2000);

		int GetMonth() const;	
		int GetDay() const;     
		int GetYear() const;

		void Input();
		void Show() const;

		bool SetFormat(char f);

		bool Set(int m, int d, int y);

		void Increment(int numDays=1);

		int Compare(const Date& d) const;


	private:

		int day;
		int month;
		int year;
		char formatting;

		bool ValidDate(int m, int d, int y);














};
