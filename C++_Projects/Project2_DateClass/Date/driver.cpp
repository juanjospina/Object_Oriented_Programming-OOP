////////////////////////////////
//// Bob Myers
////
//// sample.cpp -- sample test program starter for Date class
///////////////////////////////

#include <iostream>
#include "date.h"

using namespace std;

int main()
{
	Date d1,d2;

	d1.Input();
	cout<<"Your date entered is: " ;
	d1.Show();

	d2.Input();
	cout<<"Your date entered is: " ;
	d2.Show();

	cout << "\n";

	cout << "Which date is greater:\n-1 d1 comes first \n"
		<< "0 Same Date \n" << "1 d2 comes first \n";

	cout << "ANSWER: " << d1.Compare(d2)<<"\n"; 




	/*	d1.SetFormat('T');
		cout << endl;
		d1.Show();
		d1.Show();
		cout << endl;
		d1.SetFormat('L');
		d1.Show();
		cout <<endl;

		d1.Increment(366);
		cout << "New Date is ";
		d1.Show();
		cout << "Test :" << d1.GetMonth()<<"/"<<d1.GetDay()<<"/"<<d1.GetYear()<<"\n";

*/	


	// should default to 1/1/2000
	/*	Date d2(4,10,1992);  // should init to 4/10/1992
		Date d3(2,31,1990);

		cout <<"month: "<< d1.GetMonth() << "\n";
		cout <<"day: " << d1.GetDay() << "\n";
		cout <<"year: "<< d1.GetYear() << "\n";

		cout << endl;

		cout <<"month: "<< d2.GetMonth() << "\n";
		cout <<"day: "<< d2.GetDay() << "\n";

		cout << "year: "<< d2.GetYear() << "\n";

		cout << endl;

		cout << "month: "<< d3.GetMonth() << "\n";
		cout <<"day: "<< d3.GetDay() << "\n";

		cout <<"year: "<< d3.GetYear() << "\n";




	// display dates to the screen
	cout << "\nDate d1 is: ";
	d1.Show();			
	cout << "\nDate d2 is: ";
	d2.Show();

	d1.Input();			// Allow user to enter a date for d1
	cout << "\nDate d1 is: ";
	d1.Show();			

	d1.SetFormat('L');		// change format of d1 to "Long" format
	cout << "\nDate d1 is: ";
	d1.Show();*/			// print d1 -- should show now in long format

	// and so on.  Add your own tests to fully test the class' 
	//   functionality.
}
