#include <iostream>
#include <string>
#include "date.h"

using namespace std;


Date::Date(int m, int d, int y)
{
	if(ValidDate(m,d,y)==false)
	{
		day = 1;
		month = 1;
		year = 2000;

	}else
		ValidDate(m,d,y);
}

bool Date::ValidDate(int m, int d, int y)
{
	switch(m)
	{
		case 1:case 3:case 5:case 7:case 8:case 10:case 12:

			if((d>0 && d<= 31) && (m>0 && m<=12) && (y>0))
			{	day = d;
				month = m;
				year = y;
				return true;
			}
			else
			{
				return false;
			}	break;

		case 4:case 6:case 9:case 11:

			if((d>0 && d<= 30) && (m>0 && m<=12) && (y>0))
			{
				day = d;
				month = m;
				year = y;
				return true;
			}
			else
			{
				return false;
			}	break;

		case 2:

			if((y%4==0 && y%100!=0)||(y%400==0))
			{
				if((d>0 && d<= 29) && (m>0 && m<=12) && (y>0))
				{
					day = d;
					month = m;
					year = y;
					return true;	
				}	
				else
					return false;

				break;

			}else
			{
				if((d>0 && d<= 28) && (m>0 && m<=12) && (y>0))
				{
					day = d;
					month = m;
					year = y;
					return true;	
				}	
				else
					return false;

				break;
			}
		default:
			return false;
			break;
	}

}

int Date::GetMonth() const
{
	return month;
}

int Date::GetDay() const
{
	return day;
}


int Date::GetYear() const
{
	return year;
}


void Date::Input()
{

	int a,b,c;
	char delim;
	do
	{

		cout<<"Please enter a Valid Date: ";
		cin >> a >> delim >> b >> delim >> c;

		ValidDate(a,b,c);

	}while(ValidDate(a,b,c)==false);
}

void Date::Show() const
{

	string monthName[13] = {"nothing","Jan","Feb","Mar","Apr","May",
		"June","July","Aug","Sept","Oct","Nov","Dec"};

	switch(formatting)
	{

		case 'T':
			cout.fill('0');
			cout.width(2);
			cout<<month<<"/";

			cout.fill('0');
			cout.width(2);

			cout<<day<<"/";

			cout.fill('0');
			cout.width(2);
			cout<<year%100<<endl;
			break;

		case 'L':
			cout<<monthName[month]<<" " << day<<", "<<year<<endl;
			break;

		case 'J':
			
			cout.fill('0');
			cout.width(2);
			cout<<year%100<<"-";//ACUERDATE ELIMINAR LOS ENDL
					     // DE TODOS ESTAS OPCIONES.
			cout.fill('0');
			cout.width(3);
			cout<<day<<endl;
			
			break;

		case 'D': default:
			cout << month << "/"<<day<<"/"<<year<<endl;
			break;
	}

}


bool Date::SetFormat(char f)
{
	if(f == 'T' || f == 'L' || f == 'D' || f == 'J')
	{
		formatting = f;
		return true;
	}else
		return false;
}

bool Date::Set(int m, int d, int y)
{
	if(ValidDate(m,d,y)==true)
		return true;
	else
		return false;
}


void Date::Increment(int numDays)
{

	for (int i=0;i<numDays;i++)
	{
		if(ValidDate(month,day,year)==true)
		{
			day = day + 1;
			if(ValidDate(month,day,year)==false)
			{
				day=1;
				if(month == 12)
				{
					month = 1;
					day = 1;
					year++;

				}else
					month++;
			}

		}
	}

}


int Date::Compare(const Date& d) const
{

	if(year == d.year && month == d.month && day == d.month)
		return 0;
	else
	{
		if(year<d.year)
			return -1;
		else if(year>d.year)
			return 1;
		else
		{
			if(month<d.month)
				return -1;
			else if(month>d.month)
				return 1;
			else
			{
				if(day<d.day)
					return-1;
				else
					return 1;
			}
		}	
	}
}
