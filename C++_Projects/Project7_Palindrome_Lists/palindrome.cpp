/**********************************************************/
// Project:  Palindrome
// Author:   Juan Jose Ospina Casas
// Email:    jjo11e@my.fsu.edu
// Course :  COP3330
// Project:  7	
// Due Date: 07/28/2014
// Summary:  This program uses a stack structure in order to  
//           determine if a string is a palindrome. The program
//           ignores spaces and punctuation.
// Output:   Tells the user if the string given is a palindrome.
/**********************************************************/

#include <iostream>  
#include <cstring>  
#include <cctype> 
#include "stack.h"

using namespace std;

void RemoveNonAlphaToLow(char *cstr);

/************************************************
 * main ()
 *
 * Author: Juan Jose Ospina
 * Date : 07/25/2014
 *
 * Description: Performs the I/O of the strings and 
 *		the manipulation of the stack. 
 *		Finally compares the 2 strings.
 ************************************************/
int main()
{
	Stack <char>  LetterBox;

	char sentence[101];
	char sentTemp [101];
	char sentence2[101];

	cout << "Please enter a string: \n> ";
	cin.getline(sentence,100);

	strcpy(sentTemp,sentence);	// Copy Input to Temp. array

	RemoveNonAlphaToLow(sentTemp);	// Delete Non-alpha chars from array.

	int sentTempLength = strlen(sentTemp); 

	for (int i=0;i<sentTempLength;i++)
		LetterBox.push(tolower(sentTemp[i]));		

	int i=0;
	while(LetterBox.isStackEmpty() != true)
	{
		LetterBox.pop(sentence2[i]);
		i++;
	}

	sentence2[i]='\0';

	if (strcmp(sentTemp,sentence2) == 0)
		cout << "\"" <<  sentence << "\""<< " IS a palindrome.\n";
	else
		cout << "\"" << sentence << "\"" << " is NOT palindrome.\n";

}


/************************************************
 * RemoveNonAlphaToLow (char* cstr)
 *
 * Author: Juan Jose Ospina
 * Date : 07/25/2014
 *
 * Description: Removes all non-alpha characters
 *              from the c-string and converts them
 *		into lower case. 
 ************************************************/
void RemoveNonAlphaToLow(char *cstr)
{
	int i = 0;
	int j = 0;
	char letter;

	while ((letter = cstr[i++]) != '\0')
	{
		if (isalpha(letter))
		{
			cstr[j++] = tolower(letter);
		}
	}
	cstr[j] = '\0';
}
