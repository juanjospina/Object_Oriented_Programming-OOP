#include <iostream>
#include "myint.h"
#include <cstring>
#include <math.h>// pow() function for extraction algorithm in constructor.
#include <cctype>

using namespace std;

/************************************************
 *	C2I 
 *
 *	Author: Robert Myers (Extracted from Assigment 5 page).
 *
 *	Description: Converts character into integer 
 *	(returns -1 for error).
 *************************************************/
int C2I(char c)
{
	if (c < '0' || c > '9')	return -1;	// error
	return (c - '0');				// success
}

/************************************************
 *	I2C
 *
 *	Author: Robert Myers (Extracted from Assigment 5 page).
 *
 *	Description:  Converts single digit integer into character 
 *	(returns '\0' for error)
 *************************************************/
char I2C(int x)
{
	if (x < 0 || x > 9)		return '\0';	// error
	return (static_cast<char>(x) + '0'); 	// success
}

// Add in operator overload and member function definitions 

//--------------------------------------------------------
//-------------------------------------------------------
/************************************************
 *	Less Than operator < 
 *
 *	Author: Juan Jose Ospina
 *	Date : 06/30/2014
 *
 *	Description: Less than comparison.
 *************************************************/
bool operator<(const MyInt& x, const MyInt& y)
{
	if(x.GetMaxSize() < y.GetMaxSize())
		return true;
	else if(y.GetMaxSize() < x.GetMaxSize())
		return false;
	else
	{
		int i=0;
		bool out = false;

		while(i < x.GetMaxSize() && out == false )
		{
			if(x.digits[i] < y.digits[i])
			{	
				out=true;
				return true;
			}
			else if (y.digits[i] < x.digits[i])
			{
				out = true;
				return false;
			}else
				out = false;

			i++;
		}
	}
}

/************************************************
 *	Greater Than operator > 
 *
 *	Author: Juan Jose Ospina
 *	Date : 06/30/2014
 *
 *	Description: Greater than comparison.
 ************************************************/
bool operator>(const MyInt& x, const MyInt& y)
{
	return (y < x);
}

/************************************************
 *	Less Than or Equal operator <= 
 *
 *	Author: Juan Jose Ospina
 *	Date : 06/30/2014
 *
 *	Description: Less than or equal comparison.
 ************************************************/
bool operator<=(const MyInt& x, const MyInt& y)
{
	return !(y < x);
}

/************************************************
 *	Greater Than or Equal operator >= 
 *
 *	Author: Juan Jose Ospina
 *	Date : 06/30/2014
 *
 *	Description: Greater than or equal comparison.
 ************************************************/
bool operator>=(const MyInt& x, const MyInt& y)
{
	return !(x < y);
}

/************************************************
 *	is Equal operator == 
 *
 *	Author: Juan Jose Ospina
 *	Date : 06/30/2014
 *
 *	Description: Equal comparison.
 ***********************************************/
bool operator==(const MyInt& x, const MyInt& y)
{
	return (!(x < y) && !(y < x));
}

/************************************************
 *	Not Equal operator != 
 *
 *	Author: Juan Jose Ospina
 *	Date : 06/30/2014
 *
 *	Description: Not equal comparison.
 ************************************************/
bool operator!=(const MyInt& x, const MyInt& y)
{
	return ((x < y) || (y < x));

}

/************************************************
 *	Insertion Operator << 
 *
 *	Author: Juan Jose Ospina
 *	Date : 06/29/2014
 *
 *	Description: Let's the user to insert the 
 *	object.
 ************************************************/
ostream& operator<<(ostream& os, const MyInt& m)
{
	for (int i=0; i<m.GetMaxSize(); i++) 
	{
		os << m.digits[i];
	}
	return os;
}

/************************************************
 *	Extraction Operator >> 
 *
 *	Author: Juan Jose Ospina
 *	Date : 07/01/2014
 *
 *	Description: Let's the user to extract the 
 *	object.
 ************************************************/
istream& operator>>(istream& inm, MyInt& m)
{

	char charstor ;
	int intstor = 0;
	int i = 0;

	while (isspace(inm.peek()))
		inm.ignore(1,' ');

	if ( (inm.peek() >= '0') && (inm.peek() <= '9') )
	{

		charstor = inm.get();
		intstor = C2I(charstor);
		m.digits[i] = intstor;
		i++;
		m.Grow(i);		

		while((inm.peek() >= '0') && (inm.peek() <= '9'))
		{
			charstor = inm.get();
			intstor = C2I(charstor);
			m.digits[i] = intstor;
			i++;

			if((inm.peek() >= '0') && (inm.peek() <= '9'))
				m.Grow(i);
		} 	
	}
	return inm;
}


/************************************************
 *	Addition Operator + 
 *
 *	Author: Juan Jose Ospina
 *	Date : 06/30/2014
 *
 *	Description: Performs addition of two
 *	MyInt numbers.
 ************************************************/
MyInt operator+ (const MyInt& x, const MyInt& y)
{
	MyInt r;		// Result - return if needed to grow.
	MyInt Mytemp;		// Result - return if not needed to grow.
	int carry = 0;		// Store carry in the sum.
	int sum = 0;		// Store the full int of the sum.
	int diffdig = 0; 	// Difference-size of bigger & smaller MyInt 
	int lessSize = 0;	// Stores size of smaller MyInt. 
	int uMax = 0;		// Stores size of bigger MyInt.
	bool boolcheck = true;	// Checks which case (which MyInt is bigger).

	// Case x is Bigger.	
	if (x.GetMaxSize() > y.GetMaxSize())
	{
		diffdig = x.GetMaxSize() - y.GetMaxSize(); 
		lessSize = y.GetMaxSize();
		uMax = x.GetMaxSize();
		boolcheck = true;
		Mytemp = x;
	}

	// Case y is Bigger.
	else if (y.GetMaxSize() > x.GetMaxSize())
	{	
		diffdig = y.GetMaxSize() - x.GetMaxSize(); 
		lessSize = x.GetMaxSize();
		uMax = y.GetMaxSize();
		boolcheck = false;
		Mytemp = y;

	}

	// Case they are equal. -- Later see if we can remove it!
	else
	{	
		diffdig = y.GetMaxSize() - x.GetMaxSize(); 
		lessSize = x.GetMaxSize();
		uMax = y.GetMaxSize();
		Mytemp = y;
	}


	int counterX = x.GetMaxSize();	// Stores counter digits in x. 
	int counterY = y.GetMaxSize();  // Stores counter digits in y.
	int counterTemp = Mytemp.GetMaxSize();//Stores counter digits MyTemp.
	int tempr = 0;	// Stores the temp result (last digit of sum).

	// Add Digits While the MyInt number with less digits "exists".
	for(int i=0;i<lessSize;i++)
	{
		sum = (x.digits[counterX-1] + y.digits[counterY-1]) + carry;
		tempr = sum % 10;
		Mytemp.digits[counterTemp-1] = tempr;
		carry = sum / 10;

		counterX--;
		counterY--;
		counterTemp--;	
	}


	//Add remaining Digits of the MyInt number with any carry remaining 
	int sizeTemp; // Stores number of remaining digits to add..

	if(boolcheck==true)
		sizeTemp = x.GetMaxSize() - lessSize;
	else
		sizeTemp = y.GetMaxSize() - lessSize;

	for(int i=0;i<diffdig;i++)
	{
		if(boolcheck==true)
		{
			sum = x.digits[sizeTemp-1] + carry;
			tempr = sum % 10;
			Mytemp.digits[sizeTemp-1] = tempr;		
			carry = sum /10;
			sizeTemp--;
		}else
		{
			sum = y.digits[sizeTemp-1] + carry;
			tempr = sum % 10;
			Mytemp.digits[sizeTemp-1] = tempr;	
			carry = sum /10;
			sizeTemp--;
		}
	}	

	/* Checks if the last digit addition has a carry out.
	   If carry = any number grows r array and copy in it
	   all the digits from the Mytemp array.
	   */

	if(carry != 0)
	{
		r.Grow(uMax);

		for(int i=0;i<Mytemp.GetMaxSize();i++)
			r.digits[i+1] = Mytemp.digits[i];

		r.digits[0] = carry;
		return r;
	}else
		return Mytemp;
}






MyInt operator* (const MyInt& x, const MyInt& y)
{

	int carry = 0;          // Store carry in the sum.
	int sum = 0;            // Store the full int of the sum.
	MyInt Mytemp;   
	MyInt result;      


	int lessSize = 0;	
	int uMax = 0;
	bool boolcheck = true;

	// Case x is Bigger.	
	if (x.GetMaxSize() > y.GetMaxSize())
	{
		//diffdig = x.GetMaxSize() - y.GetMaxSize(); 
		lessSize = y.GetMaxSize();
		uMax = x.GetMaxSize();
		boolcheck = true;
		//	Mytemp = x;
		//	result = x;
		//			Mytemp.Grow(x.GetMaxSize());
	}

	// Case y is Bigger.
	else if (y.GetMaxSize() > x.GetMaxSize())
	{	
		//diffdig = y.GetMaxSize() - x.GetMaxSize(); 
		lessSize = x.GetMaxSize();
		uMax = y.GetMaxSize();
		boolcheck = false;
		//	Mytemp.Grow(y.GetMaxSize());

	}

	// Case they are equal. -- Later see if we can remove it!
	else
	{	
		//	diffdig = y.GetMaxSize() - x.GetMaxSize(); 
		lessSize = x.GetMaxSize();
		uMax = y.GetMaxSize();
		//	Mytemp.Grow(y.GetMaxSize());
	}


	//	MyInt* multiSum;
	//	multiSum = new MyInt[lessSize]; // Create a pointer to dynamic array of MyInts.
	int counterX = x.GetMaxSize();	// Stores counter digits in x.
	int counterY = y.GetMaxSize();  // Stores counter digits in y.
	//	int counterTemp = Mytemp.GetMaxSize();//Stores counter digits MyTemp.
	int tempr = 0;
	int tempLess = lessSize;
	int j=0;
	int k=0;
	cout << "Max: " << x.GetMaxSize() << endl;
	cout << "uMa: " << uMax << endl;
	while(tempLess!=0)
	{

		// Pequeno Y
		if(boolcheck = true)
		{
			counterX = x.GetMaxSize(); //Reinicio el counter x.
			k = 0; // reinicio k.
			for(int i=0;i<uMax;i++)
			{

				int power1 = pow(10,k);
				int power2 = pow(10,j);
				Mytemp=(x.digits[counterX-1]*power1)*(y.digits[counterY-1]*power2);

				counterX--;
				k++;
				result = result + Mytemp;

			}



		}

		/*	// Pequeno X
			if(boolcheck == false)
			{
			for(int i=0;i<uMax;i++)
			{
			sum=y.digits[counterY-1]*x.digits[counterX-1]+carry;
			tempr = sum % 10;
			Mytemp.dig/
			its[counterTemp-1] = tempr;
			carry = sum / 10;
			counterY--;
			counterTemp--;
			}
			}
			*/
		j++;
		counterY--;
		tempLess--;

	}

	return result;


}



//-------------------------------------------------------
// MEMBER FUNCTIONS
//-------------------------------------------------------

/************************************************
 *	GetMaxSize()
 *
 *	Author: Juan Jose Ospina
 *	Date : 06/29/2014
 *
 *	Description: Gets the size of the number
 *	store in the array.
 ************************************************/
unsigned int MyInt::GetMaxSize()const
{
	return maxSize;

}

/************************************************
 *	Constructor ( int )
 *
 *	Author: Juan Jose Ospina
 *	Date : 06/29/2014
 *
 *	Description: Creates an object receiving an integer
 *	as parameter.
 ************************************************/
MyInt::MyInt(int n) 
{

	if(n <= 0)
	{
		maxSize = 1;
		currentSize = 0;

		digits = new int [maxSize];
		digits[0] = 0;
	}else
	{
		unsigned int length = 1;
		unsigned int temp = n;

		while( temp /= 10)	
			length++;	
		// I have to use temp because,
		// using directly n changes
		// its value and affects the
		// calculation.

		maxSize = length;

		digits = new int [maxSize];	//ALLOCATION.	

		unsigned int sizeCounter = maxSize; //var need for extract alg.

		for (int i=0; i<maxSize; i++) // Extracts each digit.
		{
			int power = pow(10,sizeCounter-1);

			digits[i] = n / power % 10; 
			sizeCounter--; 

		}
	}
}

/************************************************
 *	Constructor (char *)
 *
 *	Author: Juan Jose Ospina
 *	Date : 06/29/2014
 *
 *	Description: Creates an object receiving a character
 *	array as parameter.
 ************************************************/
MyInt::MyInt(char* c) 
{
	bool isGood = true;
	maxSize = strlen(c);

	for (int i=0; i<maxSize; i++)
	{
		if(c[i] < '0' || c[i] > '9')
		{
			maxSize = 1;
			currentSize =0;	
			isGood = false;
			break;
		}

	}

	digits = new int[maxSize];

	if(isGood == false)
		digits[0] = 0;
	else
	{
		for (int i=0; i<maxSize; i++)
		{
			digits[i] = C2I(c[i]);

		}
	}
}

/************************************************
 *	Copy Constructor
 *
 *	Author: Juan Jose Ospina
 *	Date : 06/29/2014
 *
 *	Description: Makes a copy of an object..
 ************************************************/
MyInt::MyInt(const MyInt& m)
{
	maxSize = m.maxSize;
	currentSize = m.currentSize;

	digits = new int [maxSize];

	for(int i=0;i<maxSize;i++)
		digits[i] = m.digits[i];
} 

/************************************************
 *	Assigment Operator
 *	
 *	Author: Juan Jose Ospina
 *	Date : 06/29/2014
 *
 *	Description: Assigns the object.
 ************************************************/
MyInt& MyInt::operator=(const MyInt& m)
{
	if(this != &m)
	{
		delete [] digits;

		maxSize = m.GetMaxSize();

		digits = new int [maxSize];

		for(int i=0;i<maxSize;i++)
			digits[i]= m.digits[i]; 
	}
	return *this;
}

/************************************************
 *	Destructor
 *
 *	Author: Juan Jose Ospina
 *	Date : 06/29/2014
 *
 *	Description: Cleans Memory allocation.
 ************************************************/
MyInt::~MyInt()
{
	if(digits != 0)
		delete [] digits;
}

/************************************************
 *	Grow()
 *
 *	Author: Juan Jose Ospina
 *	Date : 06/29/2014
 *
 *	Description: Grows the dynamic array, i.e
 *	the number stored.
 ************************************************/
void MyInt::Grow(const unsigned int s)
{
	maxSize = s + 1 ;			// Expand to new size
	int* tempDigits = new int[maxSize];	// Allocate new Array

	for(int i=0; i<maxSize;i++)		// Copy each i into new
		tempDigits[i] = digits[i];	// Array.

	delete [] digits;			// Remove old Array.
	digits = tempDigits;			// Point old name to new array

	/*cout << "\n** Array is being resized to: " << maxSize
	  << " Allocated slots\n" << endl;	*/
}

/************************************************
 *	Pre-Increment operator ++
 *
 *	Author: Juan Jose Ospina
 *	Date : 07/01/2014
 *
 *	Description: First increase the value by one and
 *	then execute the statement.
 ************************************************/
MyInt& MyInt::operator++()
{
	*this = *this + 1;
	return *this;
}

/************************************************
 *	Post-Increment operator++
 *
 *	Author: Juan Jose Ospina
 *	Date : 07/01/2014
 *
 *	Description: First execute the statement and then 
 *	increase the value by one.
 ************************************************/
MyInt MyInt::operator++(int)
{
	MyInt temp = *this;
	*this = *this + 1;
	return temp;
}








