#include <iostream>
#include <cstring>

using namespace std;

class MyInt
{
	friend ostream& operator<<(ostream& os, const MyInt& m);
	friend istream& operator>>(istream& inmy, MyInt& m);

	friend MyInt operator+ (const MyInt& x, const MyInt& y);
	friend MyInt operator* (const MyInt& x, const MyInt& y);

	friend bool operator< (const MyInt& x, const MyInt& y);
	friend bool operator> (const MyInt& x, const MyInt& y);
	friend bool operator<= (const MyInt& x, const MyInt& y);
	friend bool operator>= (const MyInt& x, const MyInt& y);
	friend bool operator== (const MyInt& x, const MyInt& y);
	friend bool operator!= (const MyInt& x, const MyInt& y);


	public:

	MyInt(int n = 0);		 // first constructor
	MyInt(char* c);			 // second constructor - char array.

	// Rule of 3
	~MyInt();			 // Destructor
	MyInt(const MyInt& m);  	 // Copy Constructor.
	MyInt& operator=(const MyInt& m);// Assigment operator. 

	void Grow(const unsigned int);	 // Grows Dynamic Array by one.
	unsigned int GetMaxSize()const;  // Gets Max Size of MyInt.

	MyInt& operator++();
	MyInt operator++(int);


	private:

	unsigned int maxSize;		 // Maximum Size.
	int* digits;			// Pointer to dynamic array.

};
