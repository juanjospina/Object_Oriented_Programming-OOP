#include <iostream>
using namespace std;

int main()
{
   int cookies, people;
   double cpp;

   try
   {
     cout << "Enter number of people: ";
     cin >> people;
     cout << "Enter number of cookies: ";
     cin >> cookies;

     if (cookies == 0)
	throw people;
     else if (cookies < 0)
        throw static_cast<double>(people);


     cpp = cookies/static_cast<double>(people);
     cout << cookies << " cookies.\n"
	  << people << " people.\n"
	  << "You have " << cpp << " cookies per person.\n";
   }
   catch(int e)
   {
     cout << e << " people, and no cookies!\nGo buy some cookies!\n";
   }
   catch(double t)
   {
     cout << "Second catch block type double -- do we reach it?\n";
   }

   cout << "End of program.\n";

   return 0;
}