#include "student.h"

/**********************************************************/
// Student (Base Class)
// Author:   Juan Jose Ospina Casas
/**********************************************************/

/************************************************
 * Student Default Constructor ()
 *
 * Author: Juan Jose Ospina
 * Date : 07/15/2014
 *
 * Description: Initialize a default Student.
 ************************************************/
Student::Student()
{
	strcpy(firstName,"");
	strcpy(lastName,"");
	subject = ENGLISH;
}

/************************************************
 * Student Constructor (char*, char*, Course)
 *
 * Author: Juan Jose Ospina
 * Date : 07/15/2014
 *
 * Description: Initialize a Student according to
 * the parameters received.
 ************************************************/
Student::Student(char* f, char* l, Course s)
{
	strcpy(firstName, f);
	strcpy(lastName, l);
	subject = s;
}

/************************************************
 * GetFirst ()
 *
 * Author: Juan Jose Ospina
 * Date : 07/15/2014
 *
 * Description: Returns Student's first name.
 ************************************************/
const char* Student::GetFirst() const    
{
	return firstName;        
}


/************************************************
 * GetLast ()
 *
 * Author: Juan Jose Ospina
 * Date : 07/15/2014
 *
 * Description: Returns Student's last name.
 ************************************************/
const char* Student::GetLast() const      
{
	return lastName;
} 

/************************************************
 * GetSubject ()
 *
 * Author: Juan Jose Ospina
 * Date : 07/15/2014
 *
 * Description: Returns the Student's subject.
 ************************************************/
Course Student::GetSubject()const 
{
	return subject;
}


//------------------------------------------------
//------------------------------------------------

/**********************************************************/
// English (Derived Class)
// Author:   Juan Jose Ospina Casas
/**********************************************************/

/************************************************
 * English Constructor(char*,char*,Course,int,int,int,int)
 *
 * Author: Juan Jose Ospina
 * Date : 07/15/2014
 *
 * Description: Creates an English Student.
 * (Calls Student Constructor with Parameters)
 ************************************************/
English::English(char* f, char* l, Course s, int a, int p, int m, int fe)
: Student(f,l,s)
{
	attendance = a;
	project = p;
	midterm = m;
	finalexam = fe;
}

/************************************************
 * FinalAvg ()
 *
 * Author: Juan Jose Ospina
 * Date : 07/15/2014
 *
 * Description: Calculates the Final Average Grade.
 ************************************************/
void English::FinalAvg()
{
	finalAvg=(attendance*0.1)+(project*0.3)+(midterm*0.3)+(finalexam*0.3); 
}

/************************************************
 * GetFinalExam ()
 *
 * Author: Juan Jose Ospina
 * Date : 07/15/2014
 *
 * Description: Returns final exam grade.
 ************************************************/
int English::GetFinalExam()const
{
	return finalexam;
}

/************************************************
 * GetFinalAvg ()
 *
 * Author: Juan Jose Ospina
 * Date : 07/15/2014
 *
 * Description: Returns the final average grades
 ************************************************/
double English::GetFinalAvg()const
{
	return finalAvg;
}


/************************************************
 * PrintReport (ofstream&)
 *
 * Author: Juan Jose Ospina
 * Date : 07/15/2014
 *
 * Description: Print to ofstream the specific 
 * Student first name, last name, final exam,
 * final average, and letter grade. All info is 
 * formatted.
 ************************************************/
void English::PrintReport(ofstream &os)
{
	char letterGrade;

	if(finalAvg >=90)
		letterGrade = 'A';
	else if(finalAvg >= 80 && finalAvg < 90)
		letterGrade = 'B';
	else if(finalAvg >= 70 && finalAvg < 80)
		letterGrade = 'C';
	else if(finalAvg >= 60 && finalAvg < 70)
		letterGrade = 'D';
	else
		letterGrade = 'F';

	os << left;

	os <<setw(10)<< firstName <<setw(17) << lastName  
		<< finalexam << "\t" << fixed << setprecision(2)
		<< finalAvg <<"\t"<< letterGrade << "\n"; 

	os.unsetf(ios::left);
	os.unsetf(ios::fixed);
}



//------------------------------------------------
//------------------------------------------------


/**********************************************************/
// History (Derived Class)
// Author:   Juan Jose Ospina Casas
/**********************************************************/

/************************************************
 * History Constructor(char*,char*,Course,int,int,int)
 *
 * Author: Juan Jose Ospina
 * Date : 07/15/2014
 *
 * Description: Creates an History Student.
 * (Calls Student Constructor with Parameters)
 ************************************************/
History::History(char* f, char* l, Course s, int t, int m, int fe)
: Student(f,l,s)
{
	termPaper = t;
	midterm = m;
	finalexam = fe;
}

/************************************************
 * FinalAvg ()
 *
 * Author: Juan Jose Ospina
 * Date : 07/15/2014
 *
 * Description: Calculates the Final Average Grade.
 ************************************************/
void History::FinalAvg()
{
	finalAvg=(termPaper*0.25)+(midterm*0.35)+(finalexam*0.4); 
}

/************************************************
 * GetFinalExam ()
 *
 * Author: Juan Jose Ospina
 * Date : 07/15/2014
 *
 * Description: Returns final exam grade.
 ************************************************/
int History::GetFinalExam()const
{
	return finalexam;
}

/************************************************
 * GetFinalAvg ()
 *
 * Author: Juan Jose Ospina
 * Date : 07/15/2014
 *
 * Description: Returns the final average grades
 ************************************************/
double History::GetFinalAvg()const
{
	return finalAvg;

}

/************************************************
 * PrintReport (ofstream&)
 *
 * Author: Juan Jose Ospina
 * Date : 07/15/2014
 *
 * Description: Print to ofstream the specific 
 * Student first name, last name, final exam,
 * final average, and letter grade. All info is 
 * formatted.
 ************************************************/
void History::PrintReport(ofstream &os)
{
	char letterGrade;

	if(finalAvg >=90)
		letterGrade = 'A';
	else if(finalAvg >= 80 && finalAvg < 90)
		letterGrade = 'B';
	else if(finalAvg >= 70 && finalAvg < 80)
		letterGrade = 'C';
	else if(finalAvg >= 60 && finalAvg < 70)
		letterGrade = 'D';
	else
		letterGrade = 'F';

	os << left;

	os <<setw(10)<< firstName <<setw(17) << lastName  
		<< finalexam << "\t" << fixed << setprecision(2) << finalAvg  
		<<"\t"<< letterGrade << "\n"; 

	os.unsetf(ios::left);
	os.unsetf(ios::fixed);
}

//------------------------------------------------
//------------------------------------------------

/**********************************************************/
// Math (Derived Class)
// Author:   Juan Jose Ospina Casas
/**********************************************************/

/************************************************
 * Math Constructor(char*,char*,Course,int,int,int,int,int,int,int,int)
 *
 * Author: Juan Jose Ospina
 * Date : 07/15/2014
 *
 * Description: Creates an Math Student.
 * (Calls Student Constructor with Parameters)
 ************************************************/
Math::Math(char* f, char* l, Course s, int q1, int q2, int q3,int q4,int q5,
		int t1, int t2, int fe) : Student(f,l,s)
{
	quiz1 = q1;
	quiz2 = q2;
	quiz3 = q3;
	quiz4 = q4;
	quiz5 = q5;
	test1 = t1;
	test2 = t2;
	finalexam = fe;
}

/************************************************
 * FinalAvg ()
 *
 * Author: Juan Jose Ospina
 * Date : 07/15/2014
 *
 * Description: Calculates the Final Average Grade.
 * First the calculation of the quiz average is 
 * needed in order to calculate the Final Average.
 ************************************************/
void Math::FinalAvg()
{
	quizAvg = (quiz1+quiz2+quiz3+quiz4+quiz5)/5.0;

	finalAvg=(quizAvg*0.15)+(test1*0.25)+(test2*0.25)+(finalexam*0.35); 
}

/************************************************
 * GetFinalExam ()
 *
 * Author: Juan Jose Ospina
 * Date : 07/15/2014
 *
 * Description: Returns final exam grade.
 ************************************************/
int Math::GetFinalExam()const
{
	return finalexam;
}

/************************************************
 * GetFinalAvg ()
 *
 * Author: Juan Jose Ospina
 * Date : 07/15/2014
 *
 * Description: Returns the final average grades
 ************************************************/
double Math::GetFinalAvg()const
{
	return finalAvg;
}

/************************************************
 * PrintReport (ofstream&)
 *
 * Author: Juan Jose Ospina
 * Date : 07/15/2014
 *
 * Description: Print to ofstream the specific 
 * Student first name, last name, final exam,
 * final average, and letter grade. All info is 
 * formatted.
 ************************************************/
void Math::PrintReport(ofstream &os)
{

	char letterGrade;

	if(finalAvg >=90)
		letterGrade = 'A';
	else if(finalAvg >= 80 && finalAvg < 90)
		letterGrade = 'B';
	else if(finalAvg >= 70 && finalAvg < 80)
		letterGrade = 'C';
	else if(finalAvg >= 60 && finalAvg < 70)
		letterGrade = 'D';
	else
		letterGrade = 'F';


	os<< left;

	os <<setw(10)<< firstName <<setw(17) << lastName  
		<< finalexam << "\t" << fixed << setprecision(2) << finalAvg  
		<<"\t"<< letterGrade << "\n"; 

	os.unsetf(ios::left);
	os.unsetf(ios::fixed);
}



