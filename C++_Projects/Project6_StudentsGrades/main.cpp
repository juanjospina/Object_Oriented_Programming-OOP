/**********************************************************/
// Project:  Driver - Main Program
// Author:   Juan Jose Ospina Casas
// Email:    jjo11e@my.fsu.edu
// Course :  COP3330
// Project:  6	
// Due Date: 07/18/2014
// Summary:  Ask the user for an inputfile and an outputfile.
// 	     Organize the information in the inputfile and 
// 	     print it out onto the output file.
// Output:   Creates an outputfile with all the information
// 	     categorized.
/**********************************************************/

#include <iostream>
#include <fstream>
#include <cctype>
#include <cstring>
#include "student.h"

using namespace std;

void printProgram(ifstream &inputfile,ofstream &os);
void FormatHeading(ofstream &os);

/************************************************
 * main()
 *
 * Author: Juan Jose Ospina
 * Date : 07/16/2014
 *
 * Description: Performs all the process and
 * call other functions in order to open the 
 * input file, store info, and the print it
 * out into the outputfile.
 ************************************************/
int main()
{

	// Variables
	ifstream inputfile;	// Inputfile.
	char fileused[25];	//string to store the name of the file.
	ofstream os;		// Outputfile

	// Input File Procedure
	do
	{
		inputfile.clear();

		cout << "Please enter input File name : ";
		cin >> fileused;

		inputfile.open(fileused);

		if(!inputfile)
			cout << "Invalid File, Try Again.\n";

	}while(!inputfile);


	// Output File Procedure
	do
	{
		os.clear();

		cout << "Please enter output File name : ";
		cin >> fileused;		

		os.open(fileused);

		if(!os)
			cout << "Invalid File, Try Again.\n";
	}while(!os);

	// Performing Program Taks
	printProgram(inputfile,os);

	// Closing Files
	inputfile.close();
	os.close();

	cout << "Processing Complete\n";
}

/************************************************
 * printProgram(ifstream&, ofstream&)
 *
 * Author: Juan Jose Ospina
 * Date : 07/16/2014
 *
 * Description: Performs the input and output of
 * the data. Dynamic Allocation and Storage 
 * Procedures.
 ************************************************/
void printProgram(ifstream &inputfile,ofstream &os)
{
	// Variables
	int size = 0;
	int counter = 0;
	char fName[21];		// stores temp first Name
	char lName[21];		// stores temp last name
	char subj[8];		// stores temp subject reference
	Course tempSubj;	// temporal actual subject.

	inputfile >> size;

	Student** Slist = new Student*[size]; 	// Pointer to Pointers Array.

	while(counter < size)
	{
		inputfile.ignore();		//ignores a \n char at beggi.
		inputfile.getline(lName,21,',');
		inputfile.ignore();		// ignores space char at beg.
		inputfile.getline(fName,21);
		inputfile.getline(subj,8,' ');

		if(strcmp(subj,"English")==0)
			tempSubj = ENGLISH; 
		else if(strcmp(subj,"History")==0)
			tempSubj = HISTORY;
		else if(strcmp(subj,"Math")==0)
			tempSubj = MATH;	

		if(tempSubj == ENGLISH)
		{
			int att = 0;
			int pro = 0;
			int mid = 0;
			int fin = 0;

			inputfile >> att;
			inputfile >> pro;
			inputfile >> mid;
			inputfile >> fin;

			Slist[counter] = new English(fName,lName,tempSubj,att,pro,mid,fin);				

			counter++;
		}

		else if(tempSubj == HISTORY)
		{

			int term = 0;
			int mid = 0;
			int fin = 0;

			inputfile >> term;
			inputfile >> mid;
			inputfile >> fin;

			Slist[counter] = new History(fName,lName,tempSubj,term,mid,fin);				

			counter++;

		}

		else if(tempSubj == MATH)
		{

			int q1 = 0;
			int q2 = 0;
			int q3 = 0;
			int q4 = 0;
			int q5 = 0;
			int t1 = 0;
			int t2 = 0;
			int fin = 0;

			inputfile >> q1;
			inputfile >> q2;
			inputfile >> q3;
			inputfile >> q4;
			inputfile >> q5;
			inputfile >> t1;
			inputfile >> t2;
			inputfile >> fin;

			Slist[counter] = new Math(fName,lName,tempSubj,q1,q2,q3,q4,q5,t1,t2,fin);				

			counter++;

		}

	}

	//-------------------------------------------------
	//-------------------------------------------------

	// Computes the Final Avg for All Students.
	for(int i=0;i<counter;i++)
		Slist[i]->FinalAvg();

	// -------------------------------------------------
	// -------------------------------------------------

	// Prints out all students
	os << "\nStudent Grade Sumary\n"<<"--------------------\n\n";
	os << "ENGLISH CLASS\n\n";
	FormatHeading(os);

	for(int i=0;i<counter;i++)
	{
		Slist[i]->GetSubject();

		if(Slist[i]->GetSubject() == ENGLISH)
			Slist[i]->PrintReport(os);		
	}

	os << "\n\nHISTORY CLASS\n\n";
	FormatHeading(os);

	for(int i=0;i<counter;i++)
	{
		Slist[i]->GetSubject();

		if(Slist[i]->GetSubject() == HISTORY)
			Slist[i]->PrintReport(os);		
	}

	os << "\n\nMATH CLASS\n\n";
	FormatHeading(os);

	for(int i=0;i<counter;i++)
	{
		Slist[i]->GetSubject();

		if(Slist[i]->GetSubject() == MATH)
			Slist[i]->PrintReport(os);		
	}

	//---------------------------------------------------
	//---------------------------------------------------

	// Dynamic Memory Cleaning
	for(int i=0;i<counter;i++)
		delete Slist[i];	// Deallocates each element.

	delete [] Slist; 		// Deallocates Array.

}


/************************************************
 * FormatHeading
 *
 * Author: Juan Jose Ospina
 * Date : 07/16/2014
 *
 * Description: Formats the Heading for each 
 * Subclass. Name - Final Exam - Final - Letter
 ************************************************/
void FormatHeading(ofstream &os)
{
	os << left;
	os <<setw(25)<< "Student"   
		<< "Final" << "\t" << "Final"
		<<"\t"<< "Letter" << "\n";
	os <<setw(25)<< "Name"   
		<< "Exam" << "\t" << "Avg"
		<<"\t"<< "Grade" << "\n";
	os << "----------------------------------------------\n";

	os.unsetf(ios::left);
	os.unsetf(ios::fixed);
}



