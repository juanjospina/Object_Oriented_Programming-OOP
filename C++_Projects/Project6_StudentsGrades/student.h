/**********************************************************/
// Project:  Student - English, History, Math Classes
// Author:   Juan Jose Ospina Casas
// Email:    jjo11e@my.fsu.edu
// Course :  COP3330
// Project:  6	
// Due Date: 07/18/2014
// Summary:  Blueprint for the 4 classes. By using Inheritance 
//           the Student class is an abstract class and English,
//           History, and Math classes are derived from the base
//           class.
// Output:   Creates a blueprint for a Student - 'Subject' object.
/**********************************************************/

#include <iostream>
#include <cstring>
#include <cctype>
#include <iomanip>
#include <fstream>
using namespace std;


/***********************************
 * Base Class: Student 
 *
 *
 ***********************************/

#ifndef _STUDENT_H
#define _STUDENT_H

enum Course {ENGLISH, HISTORY, MATH};

class Student
{
	public:

		// Constructors
		Student();			
		Student(char* f, char* l, Course s);	

		// Accesors
		const char* GetFirst()const;	
		const char* GetLast()const;	
		Course GetSubject()const;	

		// Virtual Functions
		virtual void PrintReport(ofstream &os)=0; // Prints Info.
		virtual void FinalAvg() = 0;		  // Calculates F.Avg 

	protected:
		char firstName[21];
		char lastName[21];
		Course subject;
};


/***********************************
 * Derived Class: English 
 *
 *
 ***********************************/
class English : public Student
{
	public:
		// Constructor
		English(char* f,char* l,Course s,int a,int p,int m,int fe);

		// Accesors
		int GetFinalExam()const;
		double GetFinalAvg()const;

		void FinalAvg();		// Computes Final Avg.
		void PrintReport(ofstream &os); // Prints Student Info.

	private:
		// Grades
		int attendance;
		int project;
		int midterm;
		int finalexam;

		double finalAvg;		//Stores Final Average grade. 


};

/***********************************
 * Derived Class: History 
 *
 *
 ***********************************/
class History : public Student
{
	public:
		// Constructor
		History(char* f, char* l, Course s, int t, int m, int fe);

		// Accesors	
		int GetFinalExam()const;
		double GetFinalAvg()const;

		void FinalAvg();		// Computes Final Avg.
		void PrintReport(ofstream &os); // Prints Student Info.

	private:
		// Grades
		int termPaper;
		int midterm;
		int finalexam;

		double finalAvg;		// Stores Final Avg grade.
};

/***********************************
 * Derived Class: Math 
 *
 *
 ***********************************/
class Math : public Student
{
	public:
		// Constructor
		Math(char*,char*,Course, int,int,int,int,int,int,int,int);
		
		// Accesors
		int GetFinalExam()const;
		double GetFinalAvg()const;

		
		void FinalAvg();		// Computes Final Avg.
		void PrintReport(ofstream &os); // Prints Student Info.

	private:
		// Grades
		int quiz1;
		int quiz2;
		int quiz3;
		int quiz4;
		int quiz5;
		int test1;
		int test2;
		int finalexam;

		double quizAvg;		// Stores Quiz Average.
		double finalAvg;	// Stores Final Avg grade.
};

#endif

