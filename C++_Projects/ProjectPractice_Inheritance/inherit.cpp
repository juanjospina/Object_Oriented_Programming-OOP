#include <iostream>
using namespace std;

class AAA
{

	public:
		AAA() {w=2; r=10;};
		virtual int Fetch()
			{return r;};
		int Process() {return w;};
	
	protected:
		int w;
	private:
		int r;

};

class BBB : public AAA
{
	public:
		BBB() {y=7; s=12;};
		int Fetch()
			{return y;};
		virtual int Process() {return s;};
	
	protected:
		int y;
	private:
		int s;

};


class CCC : public BBB
{

	public:
		CCC() {x=14;};
		int Fetch()
			{return (x + AAA::Fetch());};
		int Process() {return 60;};
	
	private:
		int x;

};



class DDD : public BBB
{

	public:
		DDD() {z=-3; y=y+2;};
	protected:
		int z;

};

class EEE : public AAA 
{

	public:
		EEE() {j=15;};
		int Fetch()
			{return (j+w);};
		int Process() {return j;};
	
	private:
		int j;

};


