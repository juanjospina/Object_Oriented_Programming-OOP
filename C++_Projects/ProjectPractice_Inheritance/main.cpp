#include <iostream>
#include "inherit.cpp"
using namespace std;

int main()
{

	AAA *p1, *p2, *p3;
	BBB *p4;
	AAA a;
	CCC c;
	DDD d;

	p1 = new BBB;
	p2 = &c;
	p3 = new EEE;
	p4 = new CCC;
	
	cout << a.Fetch() << '\t' << d.Fetch() << '\n'
	     << p1->Fetch() << '\t' << p2->Fetch() << '\n'
	     << p3->Fetch() << '\n';
	cout << p3->Process() << '\t' << p4->Process() << '\n';









}
