#include <iostream>

using namespace std;

void passByValue(int x);
void passByReference(int *x);

int main()
{
    //Pointers

    int juan=12;
    int sofia=13;

    passByValue(juan);
    passByReference(&sofia);

    cout << juan << endl;
    cout << sofia << endl;
}

void passByValue(int x)
{
    x=99;
}

void passByReference(int *x)
{
    *x=77;
}
