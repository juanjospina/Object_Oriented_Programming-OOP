#include <iostream>
#include "Birthday.h"
#include "People.h"
using namespace std;

int main()
{

    Birthday birthObject(19,6,1992);


    People peopleObject("Juan Jose Ospina", birthObject);
    peopleObject.printInfo();


    return 0;
}
