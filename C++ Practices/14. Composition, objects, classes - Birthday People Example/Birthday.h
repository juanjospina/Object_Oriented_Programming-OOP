#ifndef BIRTHDAY_H
#define BIRTHDAY_H
#include <iostream>
using namespace std;

class Birthday
{
    public:

        Birthday(int d, int m, int y);
        void printDate();


    private:

    int month;
    int day;
    int year;
};

#endif
