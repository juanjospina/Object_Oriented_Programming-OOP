#include "People.h"
#include <iostream>
#include "Birthday.h"

using namespace std;

People::People(string x, Birthday bo)
: name(x), dayOfBirth(bo)
{


}

void People::printInfo()
{
    cout << name << " was born on ";
    dayOfBirth.printDate();
}
