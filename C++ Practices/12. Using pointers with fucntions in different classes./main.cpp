#include <iostream>
#include "Juan.h"

using namespace std;

int main()
{

    Juan juanObject;
    Juan *JuanPointer = &juanObject; // Creating a pointer from the class Juan, that is equal to the direction of juanObject.

    juanObject.printExcellent();
    JuanPointer->printExcellent(); // In order to access a function from a pointer, we have to use the symbol -> to call it.

    return 0;
}
