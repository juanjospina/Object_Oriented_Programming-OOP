#include <iostream>

using namespace std;

int main()
{


  try
  {
      int num1;
      cout << "Enter your first number "<<endl;
      cin >> num1;

      int num2;
      cout << "Enter your second number"<<endl;
      cin >> num2;

      if(num2==0)
      {
          throw 0;
      }else
      {
          cout << "The answer for your division is "<< num1/num2 << endl;
      }

      } catch(...) // General Default Error Messages. It doesn't need to indicate specifically the type of variable throwed by the try block
        {
            cout << "You can't divide by 0" << endl;
        }

    return 0;
}
