#include <iostream>

using namespace std;


class Enemy
{

    public:
    virtual void attack() //     virtual void attack()=0 is a Pure Virtual Function and this cause that the Enemy class becomes an Abstract Class
                          //     Pure virtual functions need to be overwrited - Mandatory

    {
            cout << " I am the default enemy attack" << endl; // It only works(activate) when there is no function on Ninja or Monster Class


    }
};


class Ninja: public Enemy
{

    public:
    void attack()
    {
        cout<<"Ninja Chop"<<endl;
    }


};


class Monster: public Enemy
{
    public:
    void attack()
    {
        cout<<"Growl attack"<<endl;
    }

};



int main()
{
    Ninja n;
    Monster m;

    Enemy *enemy1 = &n;
    Enemy *enemy2 = &m;

    enemy1->attack();
    enemy2->attack();



    return 0;
}
