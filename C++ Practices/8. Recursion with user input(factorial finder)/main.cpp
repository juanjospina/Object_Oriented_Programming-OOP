#include <iostream>

using namespace std;


int findFactorial(int x)
{


    if(x==1)
    {
        return 1;
    }
    else
    {
        return x * findFactorial(x-1);
    }
}



int main()
{
    int x;

    cout << "Write the number you wish to find its Factorial" << endl;
    cin >> x;
    cout << "The Factorial of your number is "<< findFactorial(x) << endl;
}
