#include <iostream>
#include <fstream>

using namespace std;

int main()
{

        ofstream FilePlayers("playersinfo");

        if(FilePlayers.is_open())
        {
            cout << "The file is OK" << endl;
        }

        cout << "Enter your ID, Name, and Account Number"<<endl;
        cout << "Press CTRL+Z to exit the program \n"<<endl;

        cout << "ID" << "  " << "Name" << "  "  <<"AccountNumber" << endl;
        cout << endl;

        int ID;
        string name;
        double accountnumber;

        while(cin >> ID >> name >> accountnumber)
        {
            FilePlayers << ID << ' ' << name << ' ' << accountnumber <<endl;

        }


        FilePlayers.close();


    return 0;
}
