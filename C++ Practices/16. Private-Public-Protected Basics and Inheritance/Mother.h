#ifndef MOTHER_H
#define MOTHER_H


class Mother
{
    public:
        Mother();

        void sayName();

        int publicvariable;


    protected:

    int protectedvariable;

    private:

    int privatevariable; // This ERROR appears because we never can use private variables or functions from other Classes into other Classes.

};

#endif // MOTHER_H
