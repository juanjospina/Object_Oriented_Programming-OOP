#ifndef DAUGHTER_H
#define DAUGHTER_H

#include "Mother.h"


class Daughter: public Mother //INHERITANCE - This allows me to use the function located in the Mother class. Just the ones located in public.
{
    public:
        Daughter();

        void checkProtectionVariables();

};

#endif // DAUGHTER_H
